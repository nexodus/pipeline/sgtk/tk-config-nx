################################################################################

includes:
  - ../app_locations.yml

################################################################################

# common image categories
image_categories: &image_categories
  - out
  - comp
  - dataset
  - edit
  - element
  - matte
  - mpaint
  - paint
  - playblast
  - precomp
  - render
  - roto
  - texture

write_profile.png: &write_profile_png
  file_type: png
  file_extension: png
  name: PNGs
  promote_write_knobs: [datatype]
  settings:
    channels: rgba
    datatype: 8 bit
    raw: True
    colorspace: data
  tank_type: Rendered Image
  tile_color: []

write_profile.tga: &write_profile_tga
  file_type: targa
  file_extension: tga
  name: TGAs
  promote_write_knobs: [compression]
  settings:
    channels: rgba
    compression: RLE
    raw: True
    colorspace: data
  tank_type: Rendered Image
  tile_color: []

write_profile.tif: &write_profile_tif
  file_type: tiff
  file_extension: tif
  name: TIFs
  promote_write_knobs: [datatype, compression]
  settings:
    channels: rgba
    compression: Deflate
    datatype: 8 bit
    raw: True
    colorspace: data
  tank_type: Rendered Image
  tile_color: []

write_profile.exr: &write_profile_exr
  file_type: exr
  file_extension: exr
  name: EXRs
  promote_write_knobs: [compression, autocrop, datatype]
  settings:
    colorspace: scene_linear
    autocrop: True
    channels: rgb
    compression: Zip (1 scanline)
    datatype: 16 bit half
    metadata: all metadata
  tank_type: Rendered Image
  tile_color: []

write_profile.quicktime: &write_profile_quicktime
  file_type: mov
  file_extension: mov
  name: QuickTime Movie
  promote_write_knobs:
    - mov64_fps
    - mov64_codec
    - mov_prores_codec_profile
    - mov64_dnxhd_codec_profile
    - mov64_dnxhr_codec_profile
    - mov_h264_codec_profile
    - mov64_quality
  settings:
    colorspace: data
    raw: True
    channels: rgb
    mov64_write_timecode: True
    mov64_fast_start: True
  tank_type: Movie
  tile_color: []

write_profile.dpx: &write_profile_dpx
  file_type: dpx
  file_extension: dpx
  name: DPXs
  promote_write_knobs: [datatype]
  settings:
    channels: rgb
    datatype: "16 bit"
    colorspace: compositing_log
  tank_type: Rendered Image
  tile_color: []

write_profile.jpg: &write_profile_jpg
  file_type: jpeg
  file_extension: jpg
  name: JPGs
  promote_write_knobs: []
  settings:
    raw: True
    colorspace: data
    channels: rgb
    _jpeg_quality: 1.0
    _jpeg_sub_sampling: "4:4:4"
  tank_type: Rendered Image
  tile_color: []

write_profile.smartvectors: &write_profile_smartvectors
  file_type: exr
  file_extension: exr
  name: SmartVectors
  promote_write_knobs: []
  settings:
    colorspace: data
    channels: all
    compression: Zip (1 scanline)
    datatype: 32 bit float
    metadata: all metadata except input/*
    interleave: channels
    write_full_layer_names: True
  tank_type: Rendered Image
  tile_color: []

# asset
settings.tk-nuke-writenode.asset:
  template_script_work: asset_nuke_work_file
  show_convert_actions: True
  image_categories: *image_categories
  location: "@apps.tk-nuke-writenode.location"
  write_nodes:
    - <<: *write_profile_exr
      proxy_publish_template:
      proxy_render_template:
      render_template: asset_generic_work_img_rep_seq
      publish_template: asset_generic_pub_img_rep_seq
    - <<: *write_profile_png
      proxy_publish_template:
      proxy_render_template:
      render_template: asset_generic_work_img_rep_seq
      publish_template: asset_generic_pub_img_rep_seq
    - <<: *write_profile_tga
      proxy_publish_template:
      proxy_render_template:
      render_template: asset_generic_work_img_rep_seq
      publish_template: asset_generic_pub_img_rep_seq
    - <<: *write_profile_tif
      proxy_publish_template:
      proxy_render_template:
      render_template: asset_generic_work_img_rep_seq
      publish_template: asset_generic_pub_img_rep_seq
    - <<: *write_profile_jpg
      proxy_publish_template:
      proxy_render_template:
      render_template: asset_generic_work_img_rep_seq
      publish_template: asset_generic_pub_img_rep_seq
    - <<: *write_profile_dpx
      proxy_publish_template:
      proxy_render_template:
      render_template: asset_generic_work_img_rep_seq
      publish_template: asset_generic_pub_img_rep_seq
    - <<: *write_profile_smartvectors
      proxy_publish_template:
      proxy_render_template:
      render_template: asset_generic_work_img_rep_seq
      publish_template: asset_generic_pub_img_rep_seq
    - <<: *write_profile_quicktime
      proxy_publish_template:
      proxy_render_template:
      render_template: asset_generic_work_img_rep
      publish_template: asset_generic_pub_img_rep

# sequence
settings.tk-nuke-writenode.seq:
  template_script_work: seq_nuke_work_file
  show_convert_actions: True
  image_categories: *image_categories
  location: "@apps.tk-nuke-writenode.location"
  write_nodes:
    - <<: *write_profile_exr
      proxy_publish_template:
      proxy_render_template:
      render_template: seq_generic_work_img_rep_seq
      publish_template: seq_generic_pub_img_rep_seq
    - <<: *write_profile_png
      proxy_publish_template:
      proxy_render_template:
      render_template: seq_generic_work_img_rep_seq
      publish_template: seq_generic_pub_img_rep_seq
    - <<: *write_profile_tga
      proxy_publish_template:
      proxy_render_template:
      render_template: seq_generic_work_img_rep_seq
      publish_template: seq_generic_pub_img_rep_seq
    - <<: *write_profile_tif
      proxy_publish_template:
      proxy_render_template:
      render_template: seq_generic_work_img_rep_seq
      publish_template: seq_generic_pub_img_rep_seq
    - <<: *write_profile_jpg
      proxy_publish_template:
      proxy_render_template:
      render_template: seq_generic_work_img_rep_seq
      publish_template: seq_generic_pub_img_rep_seq
    - <<: *write_profile_dpx
      proxy_publish_template:
      proxy_render_template:
      render_template: seq_generic_work_img_rep_seq
      publish_template: seq_generic_pub_img_rep_seq
    - <<: *write_profile_smartvectors
      proxy_publish_template:
      proxy_render_template:
      render_template: seq_generic_work_img_rep_seq
      publish_template: seq_generic_pub_img_rep_seq
    - <<: *write_profile_quicktime
      proxy_publish_template:
      proxy_render_template:
      render_template: seq_generic_work_img_rep
      publish_template: seq_generic_pub_img_rep

# shot
## note that the options here are required fields, you cannot comment
## entire lines or the write nodes will break
settings.tk-nuke-writenode.shot:
  template_script_work: shot_nuke_work_file
  show_convert_actions: True
  image_categories: *image_categories
  location: "@apps.tk-nuke-writenode.location"
  write_nodes:
    - <<: *write_profile_exr
      proxy_publish_template:
      proxy_render_template:
      render_template: shot_generic_work_img_rep_seq
      publish_template: shot_generic_pub_img_rep_seq
    - <<: *write_profile_png
      proxy_publish_template:
      proxy_render_template:
      render_template: shot_generic_work_img_rep_seq
      publish_template: shot_generic_pub_img_rep_seq
    - <<: *write_profile_tga
      proxy_publish_template:
      proxy_render_template:
      render_template: shot_generic_work_img_rep_seq
      publish_template: shot_generic_pub_img_rep_seq
    - <<: *write_profile_tif
      proxy_publish_template:
      proxy_render_template:
      render_template: shot_generic_work_img_rep_seq
      publish_template: shot_generic_pub_img_rep_seq
    - <<: *write_profile_jpg
      proxy_publish_template:
      proxy_render_template:
      render_template: shot_generic_work_img_rep_seq
      publish_template: shot_generic_pub_img_rep_seq
    - <<: *write_profile_dpx
      proxy_publish_template:
      proxy_render_template:
      render_template: shot_generic_work_img_rep_seq
      publish_template: shot_generic_pub_img_rep_seq
    - <<: *write_profile_smartvectors
      proxy_publish_template:
      proxy_render_template:
      render_template: shot_generic_work_img_rep_seq
      publish_template: shot_generic_pub_img_rep_seq
    - <<: *write_profile_quicktime
      proxy_publish_template:
      proxy_render_template:
      render_template: shot_generic_work_img_rep
      publish_template: shot_generic_pub_img_rep
