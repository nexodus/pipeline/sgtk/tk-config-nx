# Copyright (c) 2017 Shotgun Software Inc.
#
# CONFIDENTIAL AND PROPRIETARY
#
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights
# not expressly granted therein are reserved by Shotgun Software Inc.

import os
import hou
import sgtk

HookBaseClass = sgtk.get_hook_baseclass()

# A dict of dicts organized by category, type and output file parm
_HOUDINI_OUTPUTS = {
    # rops
    hou.ropNodeTypeCategory(): {
        "alembic": "filename",  # alembic cache
        "comp": "copoutput",  # composite
        # "ifd": "vm_picture",  # mantra render node
        "opengl": "picture",  # opengl render
        "wren": "wr_picture",  # wren wireframe
    },
}

_MANTRA_NODES = ["ifd", "nxMantra"]


class HoudiniSessionCollector(HookBaseClass):
    """
    Collector that operates on the current houdini session. Should inherit from
    the basic collector hook.
    """

    @property
    def settings(self):
        """
        Dictionary defining the settings that this collector expects to receive
        through the settings parameter in the process_current_session and
        process_file methods.

        A dictionary on the following form::

            {
                "Settings Name": {
                    "type": "settings_type",
                    "default": "default_value",
                    "description": "One line description of the setting"
            }

        The type string should be one of the data types that toolkit accepts as
        part of its environment configuration.
        """

        # grab any base class settings
        collector_settings = super(HoudiniSessionCollector, self).settings or {}

        # settings specific to this collector
        houdini_session_settings = {
            "work_template": {
                "type": "template",
                "default": None,
                "description": "Template path for artist work files. Should "
                "correspond to a template defined in "
                "templates.yml. If configured, is made available"
                "to publish plugins via the collected item's "
                "properties. ",
            },
        }

        # update the base settings with these settings
        collector_settings.update(houdini_session_settings)

        return collector_settings

    def process_current_session(self, settings, parent_item):
        """
        Analyzes the current Houdini session and parents a subtree of items
        under the parent_item passed in.

        :param dict settings: Configured settings for this collector
        :param parent_item: Root item instance
        """
        # create an item representing the current houdini session
        session_item = self.collect_current_houdini_session(settings, parent_item)

        # collect the outputs from all mantra nodes
        self.collect_mantranodes(settings, session_item)

        # collect other, non-toolkit outputs to present for publishing
        self.collect_node_outputs(settings, session_item)

    def collect_current_houdini_session(self, settings, parent_item):
        """
        Creates an item that represents the current houdini session.

        :param dict settings: Configured settings for this collector
        :param parent_item: Parent Item instance

        :returns: Item of type houdini.session
        """

        publisher = self.parent
        first_frame = int(hou.playbar.frameRange().x())
        last_frame = int(hou.playbar.frameRange().y())

        # get the path to the current file
        path = hou.hipFile.path()

        if not path:
            self.logger.error(
                "Your current Houdini session could not be collected. You "
                "need to save your script to a proper workfile location."
            )
            return None

        # let the base collector get the session item
        session_item = super(HoudiniSessionCollector, self)._collect_file(
            settings, parent_item, path
        )

        if session_item:

            session_item.type_spec = "houdini.workfile"
            session_item.type_display = "Houdini Workfile"  # for UI only
            session_item.properties[
                "publish_type"
            ] = "Houdini Workfile"  # for publish entity in SG
            session_item.properties["session_frame_range"] = (first_frame, last_frame)

            self.logger.info("Collected current Houdini Workfile")
            return session_item

        self.logger.error("Your current Houdini session could not be collected.")
        return None

    def collect_node_outputs(self, settings, parent_item):
        """
        Creates items for known output nodes

        :param parent_item: Parent Item instance
        """

        for node_category in _HOUDINI_OUTPUTS:
            for node_type in _HOUDINI_OUTPUTS[node_category]:

                path_parm_name = _HOUDINI_OUTPUTS[node_category][node_type]

                # get all the nodes for the category and type
                nodes = hou.nodeType(node_category, node_type).instances()

                # iterate over each node
                for node in nodes:

                    # get the evaluated path parm value
                    path = node.parm(path_parm_name).eval()

                    # ensure the output path exists
                    if not os.path.exists(path):
                        continue

                    self.logger.info(
                        "Processing {} node: {}".format(node_type, node.path())
                    )

                    # allow the base class to collect and create the item. it
                    # should know how to handle the output path
                    item = super(HoudiniSessionCollector, self)._collect_file(
                        settings, parent_item, path
                    )

                    # the item has been created. update the display name to
                    # include the node path to make it clear to the user how it
                    # was collected within the current session.
                    item.name = "{}: {}".format(node.path(), item.name)

    def collect_mantranodes(self, settings, parent_item):
        """
        Checks for outputs in mantra nodes and mantra node wrappers

        :param parent_item: The item to parent new items to.
        """

        _PARMS = ["vm_picture", "vm_dcmfilename"]
        _NUMBERED_PARMS = ["vm_filename_plane{:d}", "vm_cryptolayeroutput{:d}"]

        # iterate thru all mantra node variations
        for mantra_rop in _MANTRA_NODES:

            node_type = hou.nodeType(hou.ropNodeTypeCategory(), mantra_rop)
            nodes = node_type.instances()

            # iterate thru each instance of the node_type
            for node in nodes:

                self.logger.info(
                    "Processing {} node: {}".format(node_type, node.path())
                )

                # iterate thru all the un-numbered parameters
                for parm_name in _PARMS:

                    # return the resolved path from the parameter
                    file_path = node.evalParm(parm_name)
                    self._process_mantra_file_path(
                        settings, parent_item, node, file_path
                    )

                # itereate thru all numbered params
                for parm_name in _NUMBERED_PARMS:

                    # lets try to iterate thru 30 of these which should be
                    # more than enough.
                    for i in range(1, 30):

                        try:
                            # return the resolved path from the parameter
                            file_path = node.evalParm(parm_name.format(i))
                            self._process_mantra_file_path(
                                settings, parent_item, node, file_path
                            )
                        except hou.OperationFailed:
                            break

    def _process_mantra_file_path(self, settings, parent_item, node, file_path):

        # let the basic collector handle the file
        item = super(HoudiniSessionCollector, self)._collect_file(
            settings, parent_item, file_path
        )

        if item:
            # the item has been created. update the display name to include
            # the node's path to make it clear to the user how it was
            # collected within the current session.
            item.name = "{}: {}".format(node.path(), item.name)

            # lets also attempt to get the width and height while we're here
            cam_node = hou.node(node.evalParm("camera"))
            if cam_node:
                item.properties["width_height"] = (
                    cam_node.evalParm("resx"),
                    cam_node.evalParm("resy"),
                )
