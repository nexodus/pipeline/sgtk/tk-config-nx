# Copyright (c) 2017 Shotgun Software Inc.
#
# CONFIDENTIAL AND PROPRIETARY
#
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights
# not expressly granted therein are reserved by Shotgun Software Inc.

import mimetypes
import os

import sgtk
from tank_vendor import six

HookBaseClass = sgtk.get_hook_baseclass()


class BasicSceneCollector(HookBaseClass):
    """
    A basic collector that handles files and general objects.

    This collector hook is used to collect individual files that are browsed or
    dragged and dropped into the Publish2 UI. It can also be subclassed by other
    collectors responsible for creating items for a file to be published such as
    the current Maya session file.

    This plugin centralizes the logic for collecting a file, including
    determining how to display the file for publishing (based on the file
    extension).

    """

    @property
    def fileseq(self):
        try:
            return self._fileseq
        except AttributeError:
            self.logger.debug("Loading 'fileseq' module from tk-framework-nx")
            nxfw = self.load_framework("tk-framework-nx_v1.x.x")
            import fileseq

            self._fileseq = fileseq
            # self._fileseq = nxfw.import_module("fileseq")
            return self._fileseq

    @property
    def common_file_info(self):
        """
        A dictionary of file type info that allows the basic collector to
        identify common production file types and associate them with a display
        name, item type, and config icon.

        The dictionary returned is of the form::

            {
                <Publish Type>: {
                    "extensions": [<ext>, <ext>, ...],
                    "icon": <icon path>,
                    "item_type": <item type>
                },
                <Publish Type>: {
                    "extensions": [<ext>, <ext>, ...],
                    "icon": <icon path>,
                    "item_type": <item type>
                },
                ...
            }

        See the collector source to see the default values returned.

        Subclasses can override this property, get the default values via
        ``super``, then update the dictionary as necessary by
        adding/removing/modifying values.
        """

        if not hasattr(self, "_common_file_info"):
            # do this once to avoid unnecessary processing
            self._common_file_info = {
                "Alias Model": {
                    "extensions": ["wire"],
                    "icon": self._get_icon_path("alias.png"),
                    "item_type": "file.alias",
                },
                "Alembic Cache": {
                    "extensions": ["abc"],
                    "icon": self._get_icon_path("alembic.png"),
                    "item_type": "file.alembic",
                },
                "3dsmax Scene": {
                    "extensions": ["max"],
                    "icon": self._get_icon_path("3dsmax.png"),
                    "item_type": "file.3dsmax",
                },
                "Hiero Project": {
                    "extensions": ["hrox"],
                    "icon": self._get_icon_path("hiero.png"),
                    "item_type": "file.hiero",
                },
                "Houdini Scene": {
                    "extensions": ["hip", "hipnc"],
                    "icon": self._get_icon_path("houdini.png"),
                    "item_type": "file.houdini",
                },
                "Maya Scene": {
                    "extensions": ["ma", "mb"],
                    "icon": self._get_icon_path("maya.png"),
                    "item_type": "file.maya",
                },
                "Filmbox": {
                    "extensions": ["fbx"],
                    "icon": self._get_icon_path("filmbox.png"),
                    "item_type": "file.filmbox",
                },
                "Nuke Script": {
                    "extensions": ["nk"],
                    "icon": self._get_icon_path("nuke.png"),
                    "item_type": "file.nuke",
                },
                "Photoshop Image": {
                    "extensions": ["psd", "psb"],
                    "icon": self._get_icon_path("photoshop.png"),
                    "item_type": "file.photoshop",
                },
                "VRED Scene": {
                    "extensions": ["vpb", "vpe", "osb"],
                    "icon": self._get_icon_path("vred.png"),
                    "item_type": "file.vred",
                },
                "Image": {
                    "extensions": [
                        "cin",
                        "dpx",
                        "exr",
                        "png",
                        "hdr",
                        "tif",
                        "tiff",
                        "tga",
                        "pic",
                        "sgi",
                        "jpeg",
                        "jpg",
                    ],
                    "icon": self._get_icon_path("image.png"),
                    "item_type": "file.image",
                },
                "Video": {
                    "extensions": [
                        "mov",
                        "mp4",
                        "m4v",
                        "avi",
                        "wmv",
                        "flv",
                        "webm",
                        "mxf",
                    ],
                    "icon": self._get_icon_path("video.png"),
                    "item_type": "file.video",
                },
                "Texture": {
                    "extensions": ["tx", "dds", "rat"],
                    "icon": self._get_icon_path("texture.png"),
                    "item_type": "file.texture",
                },
                "PDF": {
                    "extensions": ["pdf"],
                    "icon": self._get_icon_path("pdf.png"),
                    "item_type": "file.pdf",
                },
                "Object": {
                    "extensions": ["obj"],
                    "icon": self._get_icon_path("object.png"),
                    "item_type": "file.obj",
                },
                "Color LUT": {
                    "extensions": ["3dl", "blut", "cms", "csp", "cub", "cube", "vf"],
                    "icon": self._get_icon_path("lut.png"),
                    "item_type": "file.lut",
                },
                "Color Decision List": {
                    "extensions": ["cdl", "cc", "ccc"],
                    "icon": self._get_icon_path("cdl.png"),
                    "item_type": "file.cdl",
                },
            }

        return self._common_file_info

    @property
    def settings(self):
        """
        Dictionary defining the settings that this collector expects to receive
        through the settings parameter in the process_current_session and
        process_file methods.

        A dictionary on the following form::

            {
                "Settings Name": {
                    "type": "settings_type",
                    "default": "default_value",
                    "description": "One line description of the setting" }
            }

        The type string should be one of the data types that toolkit accepts as
        part of its environment configuration.
        """

        return {
            "template_map": {
                "type": "dict",
                "default": {},
                "description": "A dictionary of path template mappings with the "
                "key being the work_template name and the value set to the cooresponding "
                "publish_template.",
            }
        }

    def process_current_session(self, settings, parent_item):
        """
        Analyzes the current scene open in a DCC and parents a subtree of items
        under the parent_item passed in.

        :param dict settings: Configured settings for this collector
        :param parent_item: Root item instance
        """

        # default implementation does not do anything
        pass

    def process_file(self, settings, parent_item, path):
        """
        Analyzes the given file and creates one or more items
        to represent it.

        :param dict settings: Configured settings for this collector
        :param parent_item: Root item instance
        :param path: Path to analyze

        :returns: The main item that was created, or None if no item was created
            for the supplied path
        """

        # handle files and folders differently
        if os.path.isdir(path):
            self._collect_folder(settings, parent_item, path)
            return None
        else:
            return self._collect_file(settings, parent_item, path)

    def _collect_file(self, settings, parent_item, path):
        # publisher app instance
        publisher = self.parent

        # make sure the path is normalized. no trailing separator, separators
        # are appropriate for the current os, no double separators, etc.
        work_path = sgtk.util.ShotgunPath.normalize(path)
        is_sequence = False

        try:
            # get the fileseq object
            fileseq = self.fileseq.findSequenceOnDisk(work_path)
        except self.fileseq.FileSeqException:
            # if we fail to get the fileseq object (likely because that
            # file doesnt exist on disk) halt the collection on this item
            import traceback

            self.logger.debug(
                "These file(s) will not be collected: {}".format(work_path),
                extra={
                    "action_show_more_info": {
                        "label": "More info",
                        "tooltip": "Show more info.",
                        "text": "<pre>{}</pre>".format(traceback.format_exc()),
                    }
                },
            )

            return None

        work_path = fileseq.frame(fileseq.start())
        # store the realpath so we can check it against the publish
        # location later to ensure its not a symlink to the publish already
        # expanduser also to be super-safe
        work_realpath = os.path.realpath(os.path.expanduser(work_path))
        self.logger.debug("Collecting file(s): {}".format(fileseq.format()))

        # get the item info using the start frame since we need
        # an actual path to read mimetypes
        item_info = self._get_item_info(work_path)

        # assuming this is NOT a sequence at first...
        item_type_display = item_info["type_display"]
        item_type_spec = item_info["item_type"]
        item_icon_path = item_info["icon_path"]

        # fileseq.frameSet() will return a value even if the
        # sequence has only 1 file. Otherwise, will return None.
        # if this is a sequence of files, modify the info
        if fileseq.frameSet():
            is_sequence = True
            item_type_display = "{} Sequence".format(item_type_display)
            item_icon_path_seq = self._get_icon_path(
                "{}_sequence.png".format(item_type_spec.split(".")[1])
            )
            if not item_icon_path_seq.endswith("file.png"):
                item_icon_path = item_icon_path_seq
            item_type_spec = "{}.{}".format(item_type_spec, "sequence")

        # lets see if our path matches any work and publish templates
        templates = self._find_templates(settings, work_path)

        if templates.get("work_template"):
            item_work_template = templates.get("work_template")
            item_work_template_fields = item_work_template.get_fields(work_path)
            self.logger.debug(
                "Found work template: {}".format(item_work_template.name),
                extra={
                    "action_show_more_info": {
                        "label": "Show Template",
                        "tooltip": "Show the full path template definition",
                        "text": "<pre>{}</pre>".format(item_work_template.definition),
                    }
                },
            )
        else:
            self.logger.debug(
                "No work template found. {} will not be collected.".format(
                    fileseq.format()
                )
            )
            return None

        if templates.get("publish_template"):
            item_publish_template = templates.get("publish_template")
            self.logger.debug(
                "Found publish template: {}".format(item_publish_template.name),
                extra={
                    "action_show_more_info": {
                        "label": "Show Template",
                        "tooltip": "Show the full path template definition",
                        "text": "<pre>{}</pre>".format(
                            item_publish_template.definition
                        ),
                    }
                },
            )
        else:
            self.logger.debug(
                "No publish template found. {} will not be collected.".format(
                    fileseq.format()
                )
            )
            return None

        # lets start to get all the data we will want to put into the item
        item_path = publisher.util.get_frame_sequence_path(work_path)
        item_publish_name = publisher.util.get_publish_name(work_path)

        # get a nice display name for the item in the publisher UI
        item_name = fileseq.format("{basename}{extension}")
        if fileseq.frameRange():
            item_name = fileseq.format("{basename}[{range}]{extension}")

        # create and populate the item
        publish_item = parent_item.create_item(
            item_type_spec, item_type_display, item_name
        )
        publish_item.set_icon_from_path(item_icon_path)
        publish_item.properties["batch_name"] = item_name
        publish_item.properties["path"] = item_path
        publish_item.properties["work_realpath"] = work_realpath
        publish_item.properties["publish_name"] = item_publish_name
        publish_item.properties["publish_type"] = item_type_display
        publish_item.properties["fileseq"] = fileseq
        publish_item.properties["is_sequence"] = is_sequence
        publish_item.properties["work_template"] = item_work_template
        publish_item.properties["work_template_fields"] = item_work_template_fields
        publish_item.properties["publish_template"] = item_publish_template

        # if the supplied path is an image, use the start frame as the thumbnail.
        # if item_type_spec.startswith("file.image") or item_type_spec.startswith(
        #     "file.texture"
        # ):
        if os.path.splitext(item_path)[0] in [
            ".png",
            ".jpg",
            ".jpeg",
            ".tif",
            ".tiff",
            ".tga",
        ]:
            publish_item.set_thumbnail_from_path(fileseq.frame(fileseq.start()))
            # disable thumbnail creation since we get it for free
            publish_item.thumbnail_enabled = False

        # Presumably if we have a context.entity its coming from the dcc
        # we're running the publiher from, so we should keep that.
        # Usally in those cases context changes are not allowed anyway.
        # However, if we don't have a context.entity it's likely we're
        # using the standalone publisher which only gives a context.project.
        if not publish_item.context.entity:
            # lets attempt to get a context from the path or work_template
            item_context = self._resolve_item_context(item_path, item_work_template)
            # if we succeed, override the context on the published item
            if item_context:
                publish_item.context = item_context

        self.logger.info("Collected file: {}".format(str(publish_item)))
        self.logger.info("Collected file context: {}".format(str(publish_item.context)))

        return publish_item

    def _collect_folder(self, settings, parent_item, folder):
        for root, subdirs, files in os.walk(folder):
            # make sure the path is normalized. no trailing separator, separators
            # are appropriate for the current os, no double separators, etc.
            search_folder = sgtk.util.ShotgunPath.normalize(root)
            search_folder = os.path.realpath(os.path.expanduser(search_folder))

            self.logger.info("Collecting files in folder: {}".format(search_folder))

            # get a list of fileseq objects based on the files in the folder
            fileseqs = self.fileseq.findSequencesOnDisk(search_folder)
            if fileseqs:
                self.logger.debug(
                    "View fileseqs found in folder: {}".format(search_folder),
                    extra={
                        "action_show_more_info": {
                            "label": "Show info",
                            "tooltip": "View all fileseqs found in folder.",
                            "text": "{}".format(fileseqs),
                        }
                    },
                )

            for fileseq in fileseqs:
                # this catches a bug in the findSequencesOnDisk method because
                # it sees multiple versioned files in one folder that dont have
                # frames as part of a sequence. So here we'll take the files
                # in that 'sequence' and send them to the _collect_file()
                # method individually.
                if fileseq.basename().endswith("_v"):
                    for path in list(fileseq):
                        self._collect_file(settings, parent_item, path)
                else:
                    path = fileseq.frame(fileseq.start())
                    self._collect_file(settings, parent_item, path)

    def _get_item_info(self, path):
        """
        Return a tuple of display name, item type, and icon path for the given
        filename.

        The method will try to identify the file as a common file type. If not,
        it will use the mimetype category. If the file still cannot be
        identified, it will fallback to a generic file type.

        :param path: The file path to identify type info for

        :return: A dictionary of information about the item to create::

            # path = "/path/to/some/file.0001.exr"

            {
                "item_type": "file.image.sequence",
                "type_display": "Rendered Image Sequence",
                "icon_path": "/path/to/some/icons/folder/image_sequence.png",
            }

        The item type will be of the form `file.<type>` where type is a specific
        common type or a generic classification of the file.
        """

        publisher = self.parent

        # make sure we get the mimetype from a real path
        # and not a symlink
        path = os.path.realpath(os.path.expanduser(path))

        # extract the components of the supplied path
        file_info = publisher.util.get_file_path_components(path)
        extension = file_info["extension"]
        filename = file_info["filename"]

        # default values used if no specific type can be determined
        type_display = "File"
        item_type = "file.unknown"

        # keep track if a common type was identified for the extension
        common_type_found = False

        icon_path = None

        # look for the extension in the common file type info dict
        for display in self.common_file_info:
            type_info = self.common_file_info[display]

            if extension in type_info["extensions"]:
                # found the extension in the common types lookup. extract the
                # item type, icon name.
                type_display = display
                item_type = type_info["item_type"]
                icon_path = type_info["icon"]
                common_type_found = True
                break

        if not common_type_found:
            # no common type match. try to use the mimetype category. this will
            # be a value like "image/jpeg" or "video/mp4". we'll extract the
            # portion before the "/" and use that for display.
            (category_type, _) = mimetypes.guess_type(filename)

            if category_type:
                # mimetypes.guess_type can return unicode strings depending on
                # the system's default encoding. If a unicode string is
                # returned, we simply ensure it's utf-8 encoded to avoid issues
                # with toolkit, which expects utf-8
                category_type = six.ensure_str(category_type)

                # the category portion of the mimetype
                category = category_type.split("/")[0]

                type_display = "{} File".format(
                    category.title(),
                )
                item_type = "file.{}".format(
                    category,
                )
                icon_path = self._get_icon_path(
                    "file_{}.png".format(
                        category,
                    )
                )

        # fall back to a simple file icon
        if not icon_path:
            icon_path = self._get_icon_path("file.png")

        # everything should be populated. return the dictionary
        return dict(
            item_type=item_type,
            type_display=type_display,
            icon_path=icon_path,
        )

    def _get_icon_path(self, icon_name, icons_folders=None):
        """
        Helper to get the full path to an icon.

        By default, the app's ``hooks/icons`` folder will be searched.
        Additional search paths can be provided via the ``icons_folders`` arg.

        :param icon_name: The file name of the icon. ex: "alembic.png"
        :param icons_folders: A list of icons folders to find the supplied icon
            name.

        :returns: The full path to the icon of the supplied name, or a default
            icon if the name could not be found.
        """

        # ensure the publisher's icons folder is included in the search
        app_icon_folder = os.path.join(self.disk_location, os.pardir, "icons")

        # build the list of folders to search
        if icons_folders:
            icons_folders.append(app_icon_folder)
        else:
            icons_folders = [app_icon_folder]

        # keep track of whether we've found the icon path
        found_icon_path = None

        # iterate over all the folders to find the icon. first match wins
        for icons_folder in icons_folders:
            icon_path = os.path.join(icons_folder, icon_name)
            if os.path.exists(icon_path):
                found_icon_path = icon_path
                break

        # supplied file name doesn't exist. return the default file.png image
        if not found_icon_path:
            found_icon_path = os.path.join(app_icon_folder, "file.png")

        return found_icon_path

    def _get_image_extensions(self):
        if not hasattr(self, "_image_extensions"):
            image_file_types = ["Photoshop Image", "Image", "Texture Image"]
            image_extensions = set()

            for image_file_type in image_file_types:
                image_extensions.update(
                    self.common_file_info[image_file_type]["extensions"]
                )

            # get all the image mime type image extensions as well
            mimetypes.init()
            types_map = mimetypes.types_map
            for ext, mimetype in types_map.items():
                if mimetype.startswith("image/"):
                    image_extensions.add(ext.lstrip("."))

            self._image_extensions = list(image_extensions)

        return self._image_extensions

    def _find_templates(self, settings, file_path):
        # start a dict of templates to return
        found_templates = {}
        template_map = settings["template_map"].value

        self.logger.debug("Template map: {}".format(template_map))

        # first lets see if our path matches any templates at all
        templates = self.sgtk.templates_from_path(file_path)

        self.logger.debug(
            "Found {} templates that match the path.".format(len(templates))
        )

        # if it does exist and its got "_work_" in it, lets assume its a work template
        if templates and (templates[0].name.find("_work_") >= 0):
            found_templates["work_template"] = templates[0]

            # lets see now if theres a pub template that matches
            pb_template_name = templates[0].name.replace("_work_", "_pub_")
            publish_template = self.sgtk.templates.get(pb_template_name)

            # if so, lets add that to the dict as well.
            if publish_template:
                found_templates["publish_template"] = publish_template

            self.logger.debug(
                "Found templates using string replacement method - work: {}, publish: {}".format(
                    found_templates["work_template"].name,
                    found_templates["publish_template"].name,
                )
            )

        # check if the template matches
        elif templates and templates[0].name in template_map.keys():
            work_template = self.sgtk.templates.get(templates[0].name)
            found_templates["work_template"] = work_template

            publish_template = self.sgtk.templates.get(template_map[templates[0].name])
            found_templates["publish_template"] = publish_template

            self.logger.debug(
                "Found templates using the template map setting: {},{}".format(
                    found_templates["work_template"].name,
                    found_templates["publish_template"].name,
                )
            )

        return found_templates

    def _resolve_item_context(self, path, template):
        current_engine = self.parent.engine
        project_id = current_engine.context.project["id"]

        # first try get a contex.entity from just the path
        self.logger.debug(
            "Attempting to resolve a task context from path: {}".format(path)
        )
        item_context = self.sgtk.context_from_path(path)

        if item_context.task:
            # most excellent! this is the bestest type of context. good to go!
            self.logger.debug(
                "    Found a task context! Returning context: {}".format(item_context)
            )
            return item_context
        else:
            # otherwise, lets get dirty with it
            self.logger.debug(
                "    No task context found in path, attempting to resolve context from template ({}) and path fields".format(
                    template.name
                )
            )
            entity = None
            fields = template.get_fields(path)
            self.logger.debug(
                "    Fields resolved from template and path: {}".format(fields)
            )
            # see if there's a field we can recognize as an entity,
            # and if so try to get the entity as a dict from SG
            if fields.get("Step") and fields.get("Shot"):
                entity = self.sgtk.shotgun.find_one(
                    "Task",
                    [
                        ["project.Project.id", "is", project_id],
                        ["entity.Shot.code", "is", fields.get("Shot")],
                        ["step.Step.short_name", "is", fields.get("Step")],
                        ["sg_status_list", "is_not", "cst"],
                    ],
                    [],
                )
            elif fields.get("Step") and fields.get("Asset"):
                entity = self.sgtk.shotgun.find_one(
                    "Task",
                    [
                        ["project.Project.id", "is", project_id],
                        ["entity.Asset.code", "is", fields.get("Asset")],
                        ["step.Step.short_name", "is", fields.get("Step")],
                        ["sg_status_list", "is_not", "cst"],
                    ],
                    [],
                )
            elif fields.get("Step") and fields.get("Sequence"):
                entity = self.sgtk.shotgun.find_one(
                    "Task",
                    [
                        ["project.Project.id", "is", project_id],
                        ["entity.Sequence.code", "is", fields.get("Sequence")],
                        ["step.Step.short_name", "is", fields.get("Step")],
                        ["sg_status_list", "is_not", "cst"],
                    ],
                    [],
                )
            elif fields.get("Step") and fields.get("Show"):
                entity = self.sgtk.shotgun.find_one(
                    "Task",
                    [
                        ["project.Project.id", "is", project_id],
                        ["entity.Project.code", "is", fields.get("Show")],
                        ["step.Step.short_name", "is", fields.get("Step")],
                        ["sg_status_list", "is_not", "cst"],
                    ],
                    [],
                )
            elif fields.get("Asset"):
                entity = self.sgtk.shotgun.find_one(
                    "Asset",
                    [
                        ["project.Project.id", "is", project_id],
                        ["code", "is", fields.get("Asset")],
                    ],
                    [],
                )
            elif fields.get("Shot"):
                entity = self.sgtk.shotgun.find_one(
                    "Shot",
                    [
                        ["project.Project.id", "is", project_id],
                        ["code", "is", fields.get("Shot")],
                    ],
                    [],
                )
            elif fields.get("Sequence"):
                entity = self.sgtk.shotgun.find_one(
                    "Sequence",
                    [
                        ["project.Project.id", "is", project_id],
                        ["code", "is", fields.get("Sequence")],
                    ],
                    [],
                )
            if fields.get("Show"):
                entity = self.sgtk.shotgun.find_one(
                    "Project",
                    [
                        ["project.Project.id", "is", project_id],
                        ["code", "is", fields.get("Show")],
                    ],
                    [],
                )

            # if we found a matching SG entity, lets try to turn that
            # into a context and return it
            if entity:
                self.logger.debug(
                    "    Found a '{}' entity in the path: {}".format(
                        entity.get("type"), entity
                    )
                )
                item_context = self.sgtk.context_from_entity(
                    entity.get("type"), entity.get("id")
                )
                self.logger.debug(
                    "    Resolved context for entity: {}".format(item_context)
                )
                return item_context

        # if all else fails, just return None :(
        return None
