import pprint
import os
import sgtk
from sgtk.platform.qt import QtCore, QtGui
from datetime import datetime, timedelta
from time import sleep

HookBaseClass = sgtk.get_hook_baseclass()


class PostPhaseHook(HookBaseClass):
    """
    This hook defines methods that are executed after each phase of a publish:
    validation, publish, and finalization. Each method receives the publish
    tree instance being used by the publisher, giving full control to further
    curate the publish tree including the publish items and the tasks attached
    to them. See the :class:`PublishTree` documentation for additional details
    on how to traverse the tree and manipulate it.
    """

    def post_validate(self, publish_tree):

        # reset the publish_paths property so it's cleared for the next
        # validation run.
        publish_tree.root_item.properties["publish_paths"] = []

    def post_publish(self, publish_tree):

        # collect all dispatchers and process them all
        for item in publish_tree:

            # if publish items are unchecked we should ignore them. Otherwise,
            if item.checked:
                task = item.properties["upstream_scalar"]

                # if there's an upstream task already
                if task:

                    # get it's dispatcher, so we can process everything
                    dispatcher = getattr(task, "dispatcher")

                    # this sets the batch name in Deadline
                    try:
                        # item.get_property() doesnt work in post_publish for
                        # some reason so thats why I'm doing try/except
                        batch_name = item.properties["batch_name"]
                    except:
                        batch_name = item.name
                    dispatcher.name = "[{}] {} Publish ".format(
                        item.context.project["name"], batch_name
                    )

                    # process the task trees within this dispatcher
                    self.logger.info(
                        "Submitting remote tasks to process on the farm..."
                    )

                    result = dispatcher.process()

                    self.logger.info(
                        "Remote tasks submitted successfully.",
                        extra={
                            "action_show_more_info": {
                                "label": "Show Result",
                                "tooltip": "Show the returned data from the submission",
                                "text": "<pre>{}</pre>".format(pprint.pformat(result)),
                            }
                        },
                    )

    def post_finalize(self, publish_tree):
        pass
