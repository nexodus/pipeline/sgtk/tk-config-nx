# Copyright (c) 2017 Shotgun Software Inc.
#
# CONFIDENTIAL AND PROPRIETARY
#
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights
# not expressly granted therein are reserved by Shotgun Software Inc.

import os
import pprint

import sgtk

HookBaseClass = sgtk.get_hook_baseclass()


class BaseCreateVersionPlugin(HookBaseClass):
    """
    Plugin for creating generic versions in Shotgun.

    """

    @property
    def icon(self):
        """
        Path to an png icon on disk
        """

        # look for icon one level up from this hook's folder in "icons" folder
        return os.path.join(self.disk_location, os.pardir, "icons", "task_version.png")

    @property
    def name(self):
        """
        One line display name describing the plugin
        """
        return "Create Version for Review"

    @property
    def description(self):
        """
        Verbose, multi-line description of what the plugin does. This can
        contain simple html for formatting.
        """

        return """
        <h2>This plugin performs the following actions:</h2>

        <h3>Creates a Version in ShotGrid</h3>
        A <b>Version</b> entry will be created in ShotGrid and added to the
        Pending Review playlist.

        <h3>Generates a proxy movie</h3>
        A QuickTime movie file will be generated on the render farm and uploaded
        to the corresponding version entry in ShotGrid.
        """

    @property
    def settings(self):
        """
        Dictionary defining the settings that this plugin expects to recieve
        through the settings parameter in the accept, validate, publish and
        finalize methods.

        A dictionary on the following form::

            {
                "Settings Name": {
                    "type": "settings_type",
                    "default": "default_value",
                    "description": "One line description of the setting"
            }

        The type string should be one of the data types that toolkit accepts
        as part of its environment configuration.
        """

        return {
            "default_transcoder_path_template": {
                "type": "template",
                "default": "default_version_transcoder_template",
                "description": (
                    "This is the path to the default transcoder should there not be one available "
                    "for the current context."
                ),
            },
            "transcoder_path_template": {
                "type": "template",
                "default": "default_version_transcoder_template",
                "description": (
                    "This is the path on disk where the version media will be stored."
                ),
            },
            "version_media_extension": {
                "type": "string",
                "default": "mov",
                "description": (
                    "This is the extension for the version media being created. This is ususally a "
                    "QuickTime movie."
                ),
            },
        }

    @property
    def item_filters(self):
        """
        List of item types that this plugin is interested in.

        Only items matching entries in this list will be presented to the
        accept() method. Strings can contain glob patters such as *, for example
        ["maya.*", "file.maya"]
        """
        return ["file.image", "file.image.sequence"]

    def accept(self, settings, item):
        """
        Method called by the publisher to determine if an item is of any
        interest to this plugin. Only items matching the filters defined via the
        item_filters property will be presented to this method.

        A publish task will be generated for each item accepted here. Returns a
        dictionary with the following booleans:

            - accepted: Indicates if the plugin is interested in this value at
                all. Required.
            - enabled: If True, the plugin will be enabled in the UI, otherwise
                it will be disabled. Optional, True by default.
            - visible: If True, the plugin will be visible in the UI, otherwise
                it will be hidden. Optional, True by default.
            - checked: If True, the plugin will be checked in the UI, otherwise
                it will be unchecked. Optional, True by default.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process

        :returns: dictionary with boolean keys accepted, required and enabled
        """

        return {"accepted": True}

    def validate(self, settings, item):
        """
        Validates the given item to check that it is ok to publish.

        Returns a boolean to indicate validity.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process

        :returns: True if item is valid, False otherwise.
        """

        # --- Make sure there is an active publish_file plugin on this item

        publish_file_plugin_id = item.get_property("publish_file_plugin_id")
        self.logger.debug(
            "Looking for 'publish_file' plugin with id: {}".format(
                publish_file_plugin_id
            )
        )

        # look thru the items task list and find the one whose plugin
        # matches the publish_file instance by id. If that task is
        # active, we are good to make a version.
        for task in item.tasks:
            if hasattr(task.plugin._hook_instance, "id"):
                self.logger.debug(
                    "Found a 'publish_file' plugin with id: {}".format(
                        task.plugin._hook_instance.id
                    )
                )
                if (
                    task.plugin._hook_instance.id == publish_file_plugin_id
                    and task.active
                ):
                    self.logger.debug(
                        "Found an active 'publish_file' plugin: {}".format(task.plugin)
                    )
                    break
        else:
            self.logger.error(
                "An active 'publish_file' plugin was not found. A version "
                "cannot be created if the item is not first published."
            )
            return False

        # --- Make sure we have all the item properties we need

        publish_path = item.get_property("publish_path")
        fileseq = item.get_property("fileseq")

        if not publish_path:
            self.logger.error(
                "No 'publish_path' property found on the item. "
                "This is required for version creation."
            )
            return False

        if not fileseq:
            self.logger.error(
                "No 'fileseq' object property found on the item. "
                "This is required for version creation."
            )
            return False

        # --- Passed all checks above!
        return True

    def publish(self, settings, item):
        """
        Executes the publish logic for the given item and settings.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process
        """

        publisher = self.parent
        publish_path = item.get_property("publish_path")
        version_code = self._get_version_code(item)
        if item.get_property("frame_range"):
            first_frame, last_frame = item.get_property("frame_range")
        else:
            # default to 1001-1001 for range if there isnt one. The
            # presumption here is that this is a single image with no
            # frame seq numbers in the filename otherwise it would
            # have a frame_range from the publish_file plugin.
            first_frame, last_frame = 1001, 1001
        if item.get_property("width_height"):
            width, height = item.get_property("width_height")
        else:
            width, height = None, None

        self.logger.info("Creating Version: {}".format(version_code))
        version_data = {
            "published_files": [item.properties.sg_publish_data],
            "project": item.context.project,
            "code": version_code,
            "description": item.description,
            "entity": self._get_version_entity(item),
            "sg_task": item.context.task,
            "sg_published": True,
            "sg_first_frame": int(first_frame),
            "sg_last_frame": int(last_frame),
            "sg_width": width,
            "sg_height": height,
            "frame_range": "{}-{}".format(first_frame, last_frame),
            "frame_count": int(last_frame - first_frame + 1),
            "sg_path_to_frames": publish_path,
        }

        # log the version data for debugging
        self.logger.debug(
            "Populated Version data...",
            extra={
                "action_show_more_info": {
                    "label": "Version Data",
                    "tooltip": "Show the complete Version data dictionary",
                    "text": "<pre>{}</pre>".format(pprint.pformat(version_data)),
                }
            },
        )

        # create the ShotGrid version
        sg_version = publisher.shotgun.create("Version", version_data)
        self.logger.info("Version created!")

        fw = self.load_framework("tk-framework-deadline_v0.x.x")
        nxfw = self.load_framework("tk-framework-nx_v1.x.x")
        # scalar = nxfw.import_module("scalar")
        import scalar

        parent_task = None

        # see if a dispatcher has already been made, if so use it
        upstream_scalar = item.properties.get("upstream_scalar")
        if upstream_scalar:
            parent_task = upstream_scalar
            dispatcher = upstream_scalar.dispatcher
        else:
            dispatcher = scalar.Dispatcher.using_queue(
                "sgtk_deadline", tk_framework_deadline=fw
            )

        # get the "version_transcoder_path"
        # this is the transcode template that will get passed to transcodify
        version_transcoder_path = self._get_transcoder_path(settings, item)

        # prepare the "version_media_path"
        # this will be the output path passed to transcodify
        media_path_template = self._get_media_path_template(settings, item)

        if media_path_template.keys:
            media_fields = item.context.as_template_fields(media_path_template)
        else:
            media_fields = {}

        media_fields["version_id"] = sg_version["id"]
        media_fields["version_code"] = sg_version["code"]
        media_fields["extension"] = settings.get("version_media_extension").value

        media_path = media_path_template.apply_fields(media_fields)

        # create the transcodify task
        t1 = dispatcher.task(
            "transcodify",
            name="Creating Version Media: {}".format(os.path.basename(media_path)),
            input_files="{}:{}-{}".format(publish_path, first_frame, last_frame),
            output_file=media_path,
            template_file=version_transcoder_path,
            context=item.context,
            parent=parent_task,
        )

        # Update sg_version with data that we get from the transcoder template
        version_update_data = {}

        t1_settings = t1.template.get("settings")
        if t1_settings and t1_settings.get("range"):
            if t1_settings["range"].get("preroll"):
                if t1_settings["range"]["preroll"] == 1:
                    version_update_data["sg_movie_has_slate"] = True

        version_update_data["sg_path_to_movie"] = media_path

        sg_version_update = publisher.shotgun.update(
            "Version", sg_version["id"], version_update_data
        )

        sg_version.update(sg_version_update)

        t1.data = {"sg_version": sg_version}
        t1.save()

        # make the task to do the uploading
        t2 = dispatcher.task(
            "sg_upload",
            name="Uploading Version Media to SG: {}".format(
                os.path.basename(media_path)
            ),
            entity_type=sg_version["type"],
            entity_id=sg_version["id"],
            src=media_path,
            parent=t1,
        )

        # associate this as the last-created scalar task so you can either use it's dispatcher or make a child task
        item.properties["upstream_scalar"] = t2

        # stash the version info in the item just in case
        item.properties["sg_version_data"] = sg_version

    def finalize(self, settings, item):
        """
        Execute the finalization pass. This pass executes once
        all the publish tasks have completed, and can for example
        be used to version up files.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process
        """
        pass

    def _get_version_code(self, item):
        """
        Returns the version_code from a file path.

        :param path: Get a formatted version_code from path
        """
        publisher = self.parent
        fileseq = item.get_property("fileseq")
        first_frame_path = fileseq.frame(fileseq.start())

        return publisher.execute_hook_method(
            "path_info", "get_version_code", path=first_frame_path
        )

    def _get_media_path_template(self, settings, item):
        publisher = self.parent

        entity = self._get_version_entity(item)
        if entity.get("type") == "Project":
            template_name = "show_version_media"
        elif entity.get("type") == "Asset":
            template_name = "asset_version_media"
        elif entity.get("type") == "Sequence":
            template_name = "seq_version_media"
        elif entity.get("type") == "Shot":
            template_name = "shot_version_media"

        template = publisher.engine.get_template_by_name(template_name)

        self.logger.debug("media_path_template: {}".format(template))
        return template

    def _get_transcoder_path(self, settings, item):
        """
        Returns the path to the transcodify template.
        """
        publisher = self.parent

        # get the template names from the settings
        default_template_setting = settings.get("default_transcoder_path_template")
        template_setting = settings.get("transcoder_path_template")

        # turn those into template objects
        default_template = publisher.engine.get_template_by_name(
            default_template_setting.value
        )
        template = publisher.engine.get_template_by_name(template_setting.value)

        self.logger.debug("default transcoder template: {}".format(default_template))
        self.logger.debug("override transcoder template: {}".format(template))
        self.logger.debug("item context: {}".format(item.context))

        # create the default path
        default_path = default_template.apply_fields(
            item.properties["work_template_fields"]
        )

        # create the override path
        path = template.apply_fields(item.properties["work_template_fields"])

        # if the override path exists, use that
        if os.path.isfile(path):
            return path
        # otherwise return the default transcoder
        else:
            return default_path

    def _get_version_entity(self, item):
        """
        Returns the best entity to link the version to.
        """
        if hasattr(self, "_version_entity"):
            pass
        elif item.context.entity:
            self._version_entity = item.context.entity
        elif item.context.project:
            self._version_entity = item.context.project
        else:
            self._version_entity = None

        self.logger.debug("entity: {}".format(self._version_entity))
        return self._version_entity
