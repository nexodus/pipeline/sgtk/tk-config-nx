# Copyright (c) 2017 Shotgun Software Inc.
#
# CONFIDENTIAL AND PROPRIETARY
#
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights
# not expressly granted therein are reserved by Shotgun Software Inc.

import os
import pprint
import traceback
import uuid
import tempfile

import sgtk

from sgtk.util.filesystem import copy_file, ensure_folder_exists

HookBaseClass = sgtk.get_hook_baseclass()


class BasePublishFilePlugin(HookBaseClass):
    """
    Plugin for creating generic publishes in ShotGrid for file that already exist in a work location.
    """

    @property
    def fileseq(self):
        try:
            return self._fileseq
        except AttributeError:
            self.logger.debug("Loading 'fileseq' module from tk-framework-nx")
            nxfw = self.load_framework("tk-framework-nx_v1.x.x")
            import fileseq

            self._fileseq = fileseq
            # self._fileseq = nxfw.import_module("fileseq")
            return self._fileseq

    ############################################################################
    # standard publish plugin properties

    @property
    def icon(self):
        """
        Path to an png icon on disk
        """

        # look for icon one level up from this hook's folder in "icons" folder
        return os.path.join(self.disk_location, os.pardir, "icons", "task_publish.png")

    @property
    def name(self):
        """
        One line display name describing the plugin.

        It should be noted that this is NOT the task's name shown in the UI. That
        name is set by the 'publish_pluings' -> 'name' setting in tk-multi-publish2.yml
        """

        return "Publish File"

    @property
    def description(self):
        """
        Verbose, multi-line description of what the plugin does. This can
        contain simple html for formatting.
        """

        loader_url = "https://help.autodesk.com/view/SGSUB/ENU/?guid=SG_Supervisor_Artist_sa_integrations_sa_integrations_user_guide_html#the-loader"

        return """
        <h2>This plugin performs the following actions:</h2>

        <h3>Creates a Publish in ShotGrid</h3>
        A <b>Published File</b> entry will be
        created in ShotGrid which will include a reference to the file's current
        path on disk. Other users will be able to access the published file via
        the <b><a href=\"{}\" style=\"color:#FFF\">Loader</a></b> so long as they have access to
        the file's location on disk.

        <h3>Moves the item to the publish area</h3>
        The published item will be moved from its current location in the work area
        to the publish area on disk. A symlink back to its work area will be created
        so that any dependencies linking to the work area will not break.
        """.format(
            loader_url
        )

    @property
    def settings(self):
        """
        Dictionary defining the settings that this plugin expects to recieve
        through the settings parameter in the accept, validate, publish and
        finalize methods.

        A dictionary on the following form::

            {
                "Settings Name": {
                    "type": "settings_type",
                    "default": "default_value",
                    "description": "One line description of the setting"
            }

        The type string should be one of the data types that toolkit accepts
        as part of its environment configuration.
        """
        return {
            "File Types": {
                "type": "list",
                "default": [
                    ["Alias File", "wire"],
                    ["Alembic Cache", "abc"],
                    ["3dsmax Scene", "max"],
                    ["Hiero Project", "hrox"],
                    ["Houdini Scene", "hip", "hipnc"],
                    ["Maya Scene", "ma", "mb"],
                    ["Motion Builder FBX", "fbx"],
                    ["Nuke Script", "nk"],
                    ["Photoshop Image", "psd", "psb"],
                    ["VRED Scene", "vpb", "vpe", "osb"],
                    ["Rendered Image", "dpx", "exr"],
                    ["Texture", "tiff", "tx", "tga", "dds"],
                    ["Image", "jpeg", "jpg", "png"],
                    ["Movie", "mov", "mp4"],
                    ["PDF", "pdf"],
                ],
                "description": (
                    "List of file types to include. Each entry in the list "
                    "is a list in which the first entry is the ShotGrid "
                    "published file type and subsequent entries are file "
                    "extensions that should be associated."
                ),
            },
        }

    @property
    def item_filters(self):
        """
        List of item types that this plugin is interested in.

        Only items matching entries in this list will be presented to the
        accept() method. Strings can contain glob patters such as *, for example
        ["maya.*", "file.maya"]
        """
        return ["file.*"]

    ############################################################################
    # standard publish plugin methods

    def accept(self, settings, item):
        """
        Method called by the publisher to determine if an item is of any
        interest to this plugin. Only items matching the filters defined via the
        item_filters property will be presented to this method.

        A publish task will be generated for each item accepted here. Returns a
        dictionary with the following booleans:

            - accepted: Indicates if the plugin is interested in this value at
                all. Required.
            - enabled: If True, the plugin will be enabled in the UI, otherwise
                it will be disabled. Optional, True by default.
            - visible: If True, the plugin will be visible in the UI, otherwise
                it will be hidden. Optional, True by default.
            - checked: If True, the plugin will be checked in the UI, otherwise
                it will be unchecked. Optional, True by default.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process

        :returns: dictionary with boolean keys accepted, required and enabled
        """

        path = item.properties.path

        # log the accepted file and display a button to reveal it in the fs
        self.logger.info(
            "File publisher plugin accepted: {}".format(path),
            extra={"action_show_folder": {"path": path}},
        )

        # return the accepted info
        return {"accepted": True}

    def validate(self, settings, item):
        """
        Validates the given item to check that it is ok to publish.

        Returns a boolean to indicate validity.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process

        :returns: True if item is valid, False otherwise.
        """

        publisher = self.parent
        root_item = self._find_root_item(item)
        path = item.properties.get("path")
        work_realpath = item.properties.get("work_realpath")

        self.logger.debug(
            "Validating this item within context: {}".format(item.context)
        )

        # ---- check to see if the context entity has folders created on disk to publish to

        if item.context.entity and len(item.context.entity_locations) == 0:
            # this context has no folders on disk yet and will need to be created before
            # going any further. maybe make a button to create the folders now?
            self.logger.info(
                "This context does not have any associated folders created on disk yet. Creating them now..."
            )
            self.sgtk.create_filesystem_structure(
                item.context.entity["type"], item.context.entity["id"]
            )
            self.logger.info("Folder creation complete!")

        # ---- determine the information required to validate

        # We allow the information to be pre-populated by the collector or a
        # base class plugin. They may have more information than is available
        # here such as custom type or template settings.

        publish_path = self.get_publish_path(settings, item)
        if not publish_path:
            self.logger.error(
                "A publish_path was not provided and could not be determined "
                "from the work_template and/or publish_template found."
            )
            return False

        publish_name = self.get_publish_name(settings, item)

        # ---- check for for a valid context

        if item.context == None:
            self.logger.error("Publishing requires a context.")
            return False

        # ---- check the publish_paths stored on root_item to ensure
        #      no other items in this publish tree share the same
        #      publish path.

        publish_paths = root_item.properties.setdefault("publish_paths", [])
        if publish_path in publish_paths:
            self.logger.error(
                "Another item in this publish tree is set to publish files to the same "
                "location. Disable this item or find the duplicate and disable it."
            )
            return False

        # ---- check for conflicting publishes of this path with a status

        # Note the name, context, and path *must* match the values supplied to
        # register_publish in the publish phase in order for this to return an
        # accurate list of previous publishes of this file.
        publishes = publisher.util.get_conflicting_publishes(
            item.context,
            publish_path,
            publish_name,
            filters=["sg_status_list", "is_not", None],
        )

        if publishes:

            self.logger.debug(
                "Conflicting publishes... ",
                extra={
                    "action_show_more_info": {
                        "label": "Show Conflicts",
                        "tooltip": "Show conflicting publishes in ShotGrid",
                        "text": "<pre>{}</pre>".format(pprint.pformat(publishes)),
                    }
                },
            )

            publish_template = self.get_publish_template(settings, item)

            if "work_template" in item.properties or publish_template:

                # templates are in play and there is already a publish in SG
                # for this file path. We will raise here to prevent this from
                # happening.
                self.logger.error(
                    "Can not validate file path. There is already a publish in "
                    "ShotGrid that matches this path. Please uncheck this "
                    "plugin or save the file to a different path."
                )
                return False

            else:
                conflict_info = (
                    "If you continue, these conflicting publishes will no "
                    "longer be available to other users via the loader:<br>"
                    "<pre>{}</pre>".format(pprint.pformat(publishes))
                )
                self.logger.warning(
                    "Found {} conflicting publishes in ShotGrid".format(len(publishes)),
                    extra={
                        "action_show_more_info": {
                            "label": "Show Conflicts",
                            "tooltip": "Show conflicting publishes in ShotGrid",
                            "text": conflict_info,
                        }
                    },
                )

        # ---- check that the work_path isnt already a symlink
        #      to the publish path.

        resolved_work_path_template = self.sgtk.template_from_path(work_realpath)
        resolved_pub_path_template = self.sgtk.template_from_path(publish_path)

        if resolved_work_path_template == resolved_pub_path_template:
            self.logger.error(
                "The item's path appears to be a symlink to a publish path already. It will not be published."
            )
            return False

        # ---- If this is a file sequence, lets warn the user if there are missing files

        if item.get_property("is_sequence") and item.get_property("fileseq"):
            fileseq = item.get_property("fileseq")
            missing_frames = fileseq.invertedFrameRange()
            if missing_frames:
                item.properties["missing_frames"] = str(missing_frames)
                if not item.get_property("ignore_missing_frames"):
                    self.logger.warning(
                        "This file sequence appears to be missing frames: {}".format(
                            missing_frames
                        ),
                        extra={
                            "action_button": {
                                "label": "Ignore Missing",
                                "tooltip": "Click to ignore missing frames on this item.",
                                "callback": lambda: _ignore_missing_frames_action(item),
                            }
                        },
                    )
                    return False
                else:
                    self.logger.warning(
                        "This file sequence appears to be missing frames ({}) but will be published.".format(
                            missing_frames
                        ),
                    )

            # ---- If this item has a frame range from the session, lets warn the user if the frame ranges dont match

            # try to get a session_frame_range from this item or the parent item
            session_frame_range = item.get_property("session_frame_range")
            if not session_frame_range and item.parent:
                session_frame_range = item.parent.get_property("session_frame_range")

            publish_frame_range = self.get_publish_frame_range(settings, item)

            # we only care if both ranges exist and they dont match
            if (
                session_frame_range
                and publish_frame_range
                and (session_frame_range != publish_frame_range)
            ):
                if not item.get_property("ignore_mismatched_range"):
                    self.logger.warning(
                        "This sequence's frame range ({}-{}) does not match the current session ({}-{}).".format(
                            publish_frame_range[0],
                            publish_frame_range[1],
                            session_frame_range[0],
                            session_frame_range[1],
                        ),
                        extra={
                            "action_button": {
                                "label": "Ignore Mismatch",
                                "tooltip": "Click to ignore mismatched frame range on item.",
                                "callback": lambda: _ignore_mismatched_range_action(
                                    item
                                ),
                            }
                        },
                    )
                    return False
                else:
                    self.logger.warning(
                        "This sequence's frame range ({}-{}) does not match the current session ({}-{}) but will be published.".format(
                            publish_frame_range[0],
                            publish_frame_range[1],
                            session_frame_range[0],
                            session_frame_range[1],
                        ),
                    )

        self.logger.info("A Publish will be created in ShotGrid and linked to:")
        self.logger.info("  {}".format(publish_path))

        # store the publish_path on the root_item so we can use it for validation
        # checks on other items in the tree
        publish_paths.append(publish_path)

        # If another plugin on this item wants to find this 'publish_file_plugin'
        # for dependency reasions, we store it here
        self.id = uuid.uuid4()
        item.properties["publish_file_plugin_id"] = self.id
        item.properties["publish_path"] = publish_path

        return True

    def publish(self, settings, item):
        """
        Executes the publish logic for the given item and settings.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process
        """

        publisher = self.parent

        # ---- create a thumbnail if we dont have one

        if not item._thumbnail_path:
            thumb_path = self._thumbnail_from_icon(item._icon_path)
            item.set_thumbnail_from_path(thumb_path)

        # ---- determine the information required to publish

        # We allow the information to be pre-populated by the collector or a
        # base class plugin. They may have more information than is available
        # here such as custom type or template settings.

        publish_type = self.get_publish_type(settings, item)
        publish_name = self.get_publish_name(settings, item)
        publish_code = self.get_publish_code(settings, item)
        publish_version = self.get_publish_version(settings, item)
        publish_path = self.get_publish_path(settings, item)
        publish_dependencies_paths = self.get_publish_dependencies(settings, item)
        publish_user = self.get_publish_user(settings, item)
        publish_fields = self.get_publish_fields(settings, item)

        # there COULD be more data available on the item at this point so
        # if any if it exists, lets attempt to store it to our published
        # file entity in ShotGrid.

        if item.get_property("is_sequence"):
            publish_first_frame, publish_last_frame = self.get_publish_frame_range(
                settings, item
            )
            publish_fields["sg_first_frame"] = publish_first_frame
            publish_fields["sg_last_frame"] = publish_last_frame

        if item.get_property("width_height"):
            publish_fields["sg_width"], publish_fields["sg_height"] = item.properties[
                "width_height"
            ]

        if item.get_property("path"):
            publish_fields["sg_prepublish_path"] = {
                "local_path": item.properties["path"]
            }

        if item.get_property("missing_frames"):
            publish_fields["sg_missing_frames"] = item.properties["missing_frames"]


        if os.getenv('REZ_USED_RESOLVE'):
            publish_fields["sg_rez_environment"] = os.getenv('REZ_USED_RESOLVE')

        try:
            import sifter
            import json

            scene_info = sifter.sift()
            if scene_info:
                publish_fields["sg_workfile_dependencies"] = json.dumps(scene_info)
        except ImportError:
            self.logger.warning('Utilize `sifter` in your environment to keep track of the actual file dependencies of this scene.')


        # catch-all for any extra kwargs that should be passed to register_publish.
        publish_kwargs = self.get_publish_kwargs(settings, item)

        # if the parent item has publish data, get it id to include it in the list of
        # dependencies
        publish_dependencies_ids = []
        if "sg_publish_data" in item.parent.properties:
            publish_dependencies_ids.append(
                item.parent.properties.sg_publish_data["id"]
            )

        # handle copying of work to publish if templates are in play
        self._copy_work_to_publish(settings, item)

        # arguments for publish registration
        self.logger.info("Registering publish...")
        publish_data = {
            "tk": publisher.sgtk,
            "context": item.context,
            "comment": item.description,
            "path": publish_path,
            "name": publish_name,
            "created_by": publish_user,
            "version_number": publish_version,
            "thumbnail_path": item.get_thumbnail_as_path(),
            "published_file_type": publish_type,
            "dependency_paths": publish_dependencies_paths,
            "dependency_ids": publish_dependencies_ids,
            "sg_fields": publish_fields,
        }

        # add extra kwargs
        publish_data.update(publish_kwargs)

        # log the publish data for debugging
        self.logger.debug(
            "Populated Publish data...",
            extra={
                "action_show_more_info": {
                    "label": "Publish Data",
                    "tooltip": "Show the complete Publish data dictionary",
                    "text": "<pre>{}</pre>".format(pprint.pformat(publish_data)),
                }
            },
        )

        # create the publish and stash it in the item properties for other
        # plugins to use.
        item.properties.sg_publish_data = sgtk.util.register_publish(**publish_data)
        self.logger.info("Publish registered!")
        self.logger.debug(
            "ShotGrid Publish data...",
            extra={
                "action_show_more_info": {
                    "label": "ShotGrid Publish Data",
                    "tooltip": "Show the complete ShotGrid Publish Entity dictionary",
                    "text": "<pre>{}</pre>".format(
                        pprint.pformat(item.properties.sg_publish_data)
                    ),
                }
            },
        )

    def finalize(self, settings, item):
        """
        Execute the finalization pass. This pass executes once
        all the publish tasks have completed, and can for example
        be used to version up files.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process
        """

        publisher = self.parent

        # get the data for the publish that was just created in SG
        publish_data = item.properties.sg_publish_data

        # ensure conflicting publishes have their status cleared
        publisher.util.clear_status_for_conflicting_publishes(
            item.context, publish_data
        )

        self.logger.info("Cleared the status of all previous, conflicting publishes")

        path = item.properties.path
        self.logger.info(
            "Publish created for file: %s" % (path,),
            extra={
                "action_show_in_shotgun": {
                    "label": "Show Publish",
                    "tooltip": "Open the Publish in ShotGrid.",
                    "entity": publish_data,
                }
            },
        )

    def get_publish_template(self, settings, item):
        """
        Get a publish template for the supplied settings and item.

        :param settings: This plugin instance's configured settings
        :param item: The item to determine the publish template for

        :return: A template representing the publish path of the item or
            None if no template could be identified.
        """

        return item.get_property("publish_template")

    def get_publish_type(self, settings, item):
        """
        Get a publish type for the supplied settings and item.

        :param settings: This plugin instance's configured settings
        :param item: The item to determine the publish type for

        :return: A publish type or None if one could not be found.
        """

        # publish type explicitly set or defined on the item
        publish_type = item.get_property("publish_type")
        if publish_type:
            return publish_type

        # fall back to the path info hook logic
        publisher = self.parent
        path = item.get_property("path")
        if path is None:
            raise AttributeError("'PublishData' object has no attribute 'path'")

        # get the publish path components
        path_info = publisher.util.get_file_path_components(path)

        # determine the publish type
        extension = path_info["extension"]

        # ensure lowercase and no dot
        if extension:
            extension = extension.lstrip(".").lower()

            for type_def in settings["File Types"].value:

                publish_type = type_def[0]
                file_extensions = type_def[1:]

                if extension in file_extensions:
                    # found a matching type in settings. use it!
                    return publish_type

        # --- no pre-defined publish type found...

        if extension:
            # publish type is based on extension
            publish_type = "%s File" % extension.capitalize()
        else:
            # no extension, assume it is a folder
            publish_type = "Folder"

        return publish_type

    def get_publish_path(self, settings, item):
        """
        Get a publish path for the supplied settings and item.

        :param settings: This plugin instance's configured settings
        :param item: The item to determine the publish path for

        :return: A string representing the output path to supply when
            registering a publish for the supplied item

        Extracts the publish path via the configured work and publish templates
        if possible.
        """

        # publish type explicitly set or defined on the item
        publish_path = item.get_property("publish_path")
        if publish_path:
            return publish_path

        # fall back to template/path logic
        path = item.get_property("path")
        if path is None:
            raise AttributeError("'PublishData' object has no attribute 'path'")

        # ---- ensure templates are available
        work_template = item.properties.get("work_template")
        if not work_template:
            self.logger.error(
                "No work template set on the item. This item cannot be published without a work template."
            )
            return None

        publish_template = self.get_publish_template(settings, item)
        if not publish_template:
            return None

        work_fields = []
        publish_path = None

        # We need both work and publish template to be defined for template
        # support to be enabled.
        if work_template and publish_template:
            if work_template.validate(path):
                work_fields = work_template.get_fields(path)
                context_fields = item.context.as_template_fields(publish_template)

                self.logger.debug(
                    "Click to see the resolved context fields...",
                    extra={
                        "action_show_more_info": {
                            "label": "Template Fields",
                            "tooltip": "Show the context fields resolved from the publish_template.",
                            "text": "<pre>{}</pre>".format(
                                pprint.pformat(context_fields)
                            ),
                        }
                    },
                )

                work_fields.update(context_fields)

            missing_keys = publish_template.missing_keys(work_fields)

            if missing_keys:
                self.logger.warning(
                    "There were keys missing when attempting to resolve the "
                    "publish path: {}".format(", ".join(missing_keys))
                )
                self.logger.warning(
                    "You may need to provide a more specific context for this item."
                )
                return False
            else:
                publish_path = publish_template.apply_fields(work_fields)
                self.logger.debug(
                    "Used publish template to determine the publish path: {}".format(
                        publish_path
                    )
                )
        else:
            self.logger.debug("publish_template: {}".format(publish_template))
            self.logger.debug("work_template: {}".format(work_template))

        if not publish_path:
            publish_path = path
            self.logger.debug("Could not validate a publish template.")

        return publish_path

    def get_publish_version(self, settings, item):
        """
        Get the publish version for the supplied settings and item.

        :param settings: This plugin instance's configured settings
        :param item: The item to determine the publish version for

        Extracts the publish version via the configured work template if
        possible. Will fall back to using the path info hook.
        """

        # publish version explicitly set or defined on the item
        publish_version = item.get_property("publish_version")
        if publish_version:
            return publish_version

        # fall back to the template/path_info logic
        publisher = self.parent
        path = item.get_property("path")
        if path is None:
            raise AttributeError("'PublishData' object has no attribute 'path'")

        work_template = item.properties.get("work_template")
        work_fields = None
        publish_version = None

        if work_template:
            if work_template.validate(path):
                self.logger.debug("Work file template configured and matches file.")
                work_fields = work_template.get_fields(path)

        if work_fields:
            # if version number is one of the fields, use it to populate the
            # publish information
            if "version" in work_fields:
                publish_version = work_fields.get("version")
                self.logger.debug("Retrieved version number via work file template.")

        else:
            self.logger.debug("Using path info hook to determine publish version.")
            publish_version = publisher.util.get_version_number(path)
            if publish_version is None:
                publish_version = 1

        return publish_version

    def get_publish_name(self, settings, item):
        """
        Get the publish name for the supplied settings and item.

        :param settings: This plugin instance's configured settings
        :param item: The item to determine the publish name for

        Uses the path info hook to retrieve the publish name.
        """

        # publish name explicitly set or defined on the item
        publish_name = item.get_property("publish_name")
        if publish_name:
            return publish_name

        # fall back to the path_info logic
        publisher = self.parent
        path = item.get_property("path")
        if path is None:
            raise AttributeError("'PublishData' object has no attribute 'path'")

        if item.get_property("is_sequence"):
            is_sequence = True
        else:
            is_sequence = False

        return publisher.util.get_publish_name(path, sequence=is_sequence)

    def get_publish_code(self, settings, item):
        """
        Get the publish code for the supplied settings and item.

        :param settings: This plugin instance's configured settings
        :param item: The item to determine the publish code for
        """

        # publish code explicitly set or defined on the item
        publish_code = item.get_property("publish_code")
        if publish_code:
            return publish_code

        # fall back to the path_info logic
        publisher = self.parent
        path = item.get_property("path")
        if path is None:
            raise AttributeError("'PublishData' object has no attribute 'path'")

        if item.get_property("is_sequence"):
            is_sequence = True
        else:
            is_sequence = False

        return publisher.util.get_publish_name(path, sequence=is_sequence)

    def get_publish_dependencies(self, settings, item):
        """
        Get publish dependencies for the supplied settings and item.

        :param settings: This plugin instance's configured settings
        :param item: The item to determine the publish template for

        :return: A list of file paths representing the dependencies to store in
            SG for this publish
        """

        # local properties first
        dependencies = item.local_properties.get("publish_dependencies")

        # have to check against `None` here since `[]` is valid and may have
        # been explicitly set on the item
        if dependencies is None:
            # get from the global item properties.
            dependencies = item.properties.get("publish_dependencies")

        if dependencies is None:
            # not set globally or locally on the item. make it []
            dependencies = []

        return dependencies

    def get_publish_user(self, settings, item):
        """
        Get the user that will be associated with this publish.

        If publish_user is not defined as a ``property`` or ``local_property``,
        this method will return ``None``.

        :param settings: This plugin instance's configured settings
        :param item: The item to determine the publish template for

        :return: A user entity dictionary or ``None`` if not defined.
        """
        return item.get_property("publish_user", default_value=None)

    def get_publish_fields(self, settings, item):
        """
        Get additional fields that should be used for the publish. This
        dictionary is passed on to :meth:`tank.util.register_publish` as the
        ``sg_fields`` keyword argument.

        If publish_fields is not defined as a ``property`` or
        ``local_property``, this method will return an empty dictionary.

        :param settings: This plugin instance's configured settings
        :param item: The item to determine the publish template for

        :return: A dictionary of field names and values for those fields.
        """
        return item.get_property("publish_fields", default_value={})

    def get_publish_kwargs(self, settings, item):
        """
        Get kwargs that should be passed to :meth:`tank.util.register_publish`.
        These kwargs will be used to update the kwarg dictionary that is passed
        when calling :meth:`tank.util.register_publish`, meaning that any value
        set here will supersede a value already retrieved from another
        ``property`` or ``local_property``.

        If publish_kwargs is not defined as a ``property`` or
        ``local_property``, this method will return an empty dictionary.

        :param settings: This plugin instance's configured settings
        :param item: The item to determine the publish template for

        :return: A dictionary of kwargs to be passed to
                 :meth:`tank.util.register_publish`.
        """
        return item.get_property("publish_kwargs", default_value={})

    def get_publish_frame_range(self, settings, item):
        """
        :param settings: This plugin instance's configured settings
        :param item: The item to determine the publish template for

        :return: A tuple of (first_frame, last_frame) or None
        """

        frame_range = item.get_property("frame_range")

        if frame_range and frame_range is tuple:
            return frame_range

        if item.get_property("is_sequence") and item.get_property("fileseq"):
            fileseq = item.get_property("fileseq")
            frame_range = (fileseq.start(), fileseq.end())
            item.properties["frame_range"] = frame_range

        return item.get_property("frame_range")

    ############################################################################
    # protected methods

    def _copy_work_to_publish(self, settings, item):
        """
        This method handles copying work file path(s) to a designated publish
        location.

        This method requires a "work_template" and a "publish_template" be set
        on the supplied item.

        The method will handle copying the "path" property to the corresponding
        publish location assuming the path corresponds to the "work_template"
        and the fields extracted from the "work_template" are sufficient to
        satisfy the "publish_template".

        The method will not attempt to copy files if any of the above
        requirements are not met. If the requirements are met, the file will
        ensure the publish path folder exists and then copy the file to that
        location.

        If the item has "sequence_paths" set, it will attempt to copy all paths
        assuming they meet the required criteria with respect to the templates.

        """

        # ---- gather templates
        work_template = item.properties.get("work_template")
        publish_template = self.get_publish_template(settings, item)

        # ---- get a list of files to be copied

        # coverting the fileseq object to a list yields a list
        # of all the files, even if its just a singleton file
        work_files = list(item.properties.fileseq)

        # ---- copy the work files to the publish location

        # import the NX framework
        nxfw = self.load_framework("tk-framework-nx_v1.x.x")
        nxfs = nxfw.import_module("utils.filesystem")

        # create a list of src/dst pairs for the move
        src_dest_pairs = []

        for work_file in work_files:

            if not work_template.validate(work_file):
                self.logger.warning(
                    "Work file '{}' did not match work template '{}'. ".format(
                        work_file, work_template
                    )
                )
                return

            work_fields = work_template.get_fields(work_file)

            missing_keys = publish_template.missing_keys(work_fields)

            if missing_keys:
                error_msg = (
                    "Work file '{}' missing keys required for the publish "
                    "template: {}".format(work_file, missing_keys)
                )
                self.logger.error(error_msg)
                raise Exception(error_msg)

            publish_file = publish_template.apply_fields(work_fields)

            # stage copying of the file
            publish_folder = os.path.dirname(publish_file)
            ensure_folder_exists(publish_folder)

            # add to the list of src/dst pairs
            if os.path.realpath(work_file) is not os.path.realpath(publish_file):
                src_dest_pairs.append(
                    {"src": str(os.path.realpath(work_file)), "dst": str(publish_file)}
                )

        self.logger.debug(
            "Staged src/dst pairs for remote copying.",
            extra={
                "action_show_more_info": {
                    "label": "View src/dst pairs",
                    "tooltip": "View the full src/dst list",
                    "text": "<pre>{}</pre>".format(pprint.pformat(src_dest_pairs)),
                }
            },
        )

        # prepare task for remote execution
        # scalar = nxfw.import_module("scalar")
        from scalar.dispatcher import Dispatcher
        dlfw = self.load_framework("tk-framework-deadline_v0.x.x")

        dispatcher = Dispatcher.using_queue(
            "sgtk_deadline", tk_framework_deadline=dlfw
        )
        dispatcher.priority_tier = "publishing"

        work_path = item.get_property("path")
        publish_path = self.get_publish_path(settings, item)

        # fallback name
        dispatcher.name = str(item.context.entity["name"])
        t1 = dispatcher.task(
            "move_files",
            name="Moving {} file(s) to publish area: {}".format(
                len(src_dest_pairs),
                os.path.basename(work_path),
            ),
            src_dest_pairs=src_dest_pairs,
        )

        # providing the first_/last_frame properties signals the task pre-submission to set the deadline job as such
        t1.first_frame = item.properties.fileseq.start()
        t1.last_frame = item.properties.fileseq.end()

        # register this as scalar's most recent task, so that each next step can refer to the it's dependencies.
        # when we use this later, we'll process the whole tree by accessing the tasks's dispatcher
        item.properties["upstream_scalar"] = t1
        item.properties["work_template_fields"] = work_fields

    def _get_next_version_info(self, path, item):
        """
        Return the next version of the supplied path.

        If templates are configured, use template logic. Otherwise, fall back to
        the zero configuration, path_info hook logic.

        :param str path: A path with a version number.
        :param item: The current item being published

        :return: A tuple of the form::

            # the first item is the supplied path with the version bumped by 1
            # the second item is the new version number
            (next_version_path, version)
        """

        if not path:
            self.logger.debug("Path is None. Can not determine version info.")
            return None, None

        publisher = self.parent

        # if the item has a known work file template, see if the path
        # matches. if not, warn the user and provide a way to save the file to
        # a different path
        work_template = item.properties.get("work_template")
        work_fields = None

        if work_template:
            if work_template.validate(path):
                work_fields = work_template.get_fields(path)

        # if we have template and fields, use them to determine the version info
        if work_fields and "version" in work_fields:

            # template matched. bump version number and re-apply to the template
            work_fields["version"] += 1
            next_version_path = work_template.apply_fields(work_fields)
            version = work_fields["version"]

        # fall back to the "zero config" logic
        else:
            next_version_path = publisher.util.get_next_version_path(path)
            cur_version = publisher.util.get_version_number(path)
            if cur_version is not None:
                version = cur_version + 1
            else:
                version = None

        return next_version_path, version

    def _save_to_next_version(self, path, item, save_callback):
        """
        Save the supplied path to the next version on disk.

        :param path: The current path with a version number
        :param item: The current item being published
        :param save_callback: A callback to use to save the file

        Relies on the _get_next_version_info() method to retrieve the next
        available version on disk. If a version can not be detected in the path,
        the method does nothing.

        If the next version path already exists, logs a warning and does
        nothing.

        This method is typically used by subclasses that bump the current
        working/session file after publishing.
        """

        (next_version_path, version) = self._get_next_version_info(path, item)

        if version is None:
            self.logger.debug(
                "No version number detected in the publish path. "
                "Skipping the bump file version step."
            )
            return None

        self.logger.info("Incrementing file version number...")

        # nothing to do if the next version path can't be determined or if it
        # already exists.
        if not next_version_path:
            self.logger.warning("Could not determine the next version path.")
            return None
        elif os.path.exists(next_version_path):
            self.logger.warning(
                "The next version of the path already exists",
                extra={"action_show_folder": {"path": next_version_path}},
            )
            return None

        # save the file to the new path
        save_callback(next_version_path)
        self.logger.info("File saved as: %s" % (next_version_path,))

        return next_version_path

    def _find_root_item(self, item):
        if item.is_root:
            return item
        else:
            return self._find_root_item(item.parent)

    def _thumbnail_from_icon(self, icon_path):

        self.logger.debug("Generating thumbnail from item icon.")

        # only import if we need it
        from sgtk.platform.qt import QtGui, QtCore

        # load the two images
        thumb_empty = QtGui.QPixmap(
            os.path.join(self.disk_location, os.pardir, "icons", "thumb_empty.png")
        )
        thumb_overlay = QtGui.QPixmap(
            os.path.join(self.disk_location, os.pardir, "icons", "thumb_overlay.png")
        )
        file_icon = QtGui.QPixmap(icon_path)

        # scale file_icon proportionally to 80% of thumb_empty's height using a better quality interpolation algorithm
        scaled_height = int(thumb_empty.height() * 0.8)
        file_icon_scaled = file_icon.scaledToHeight(
            scaled_height, QtCore.Qt.SmoothTransformation
        )

        # calculate the x and y positions of file_icon based on thumb_empty and file_icon_scaled
        x = int((thumb_empty.width() - file_icon_scaled.width()) / 2)
        y = int((thumb_empty.height() - file_icon_scaled.height()) / 2)

        # create a new image with the same dimensions as the thumb_empty image
        composite = QtGui.QPixmap(thumb_empty.size())

        # use a QPainter object to draw the thumb_empty image onto the new image with antialiasing enabled
        painter = QtGui.QPainter(composite)
        painter.setRenderHint(QtGui.QPainter.Antialiasing, True)
        painter.drawPixmap(0, 0, thumb_empty)

        # use the QPainter object again to draw the scaled file_icon image on top of the thumb_empty image at the calculated x and y positions
        painter.drawPixmap(x, y, file_icon_scaled)

        # use the QPainter one last time to comp our overlay
        painter.drawPixmap(0, 0, thumb_overlay)

        # clean up
        painter.end()

        # create a temporary file and save the composite image to it
        with tempfile.NamedTemporaryFile(suffix=".png", delete=False) as temp:
            composite.save(temp.name, "PNG")
            temp.flush()

        return temp.name


def _ignore_missing_frames_action(item):
    item.properties["ignore_missing_frames"] = True


def _ignore_mismatched_range_action(item):
    item.properties["ignore_mismatched_range"] = True
