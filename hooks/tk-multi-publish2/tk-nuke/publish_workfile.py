# Copyright (c) 2017 Shotgun Software Inc.
#
# CONFIDENTIAL AND PROPRIETARY
#
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights
# not expressly granted therein are reserved by Shotgun Software Inc.

import os
import nuke
import sgtk
from sgtk.util.filesystem import ensure_folder_exists

HookBaseClass = sgtk.get_hook_baseclass()


class NukeWorkfilePublishPlugin(HookBaseClass):
    """
    Plugin for publishing an open nuke workfile.

    This hook relies on functionality found in the base file publisher hook in
    the config and should inherit from it in the configuration. The hook
    setting for this plugin should look something like this:

        hook: "{config}/tk-multi-publish2/base/publish_file.py:{config}/tk-multi-publish2/{engine_name}/publish_workfile.py"

    """

    @property
    def description(self):
        """
        Verbose, multi-line description of what the plugin does. This can
        contain simple html for formatting.
        """

        base_desc = super(NukeWorkfilePublishPlugin, self).description

        return """{}
        <h3>Version up Workfile</h3>
        The current workfile will versioned up and saved as a new workfile.
        """.format(
            base_desc
        )

    @property
    def settings(self):
        """
        Dictionary defining the settings that this plugin expects to receive
        through the settings parameter in the accept, validate, publish and
        finalize methods.

        A dictionary on the following form::

            {
                "Settings Name": {
                    "type": "settings_type",
                    "default": "default_value",
                    "description": "One line description of the setting"
            }

        The type string should be one of the data types that toolkit accepts as
        part of its environment configuration.
        """

        # inherit the settings from the base publish plugin
        base_settings = super(NukeWorkfilePublishPlugin, self).settings or {}

        # settings specific to this class
        nuke_publish_settings = {
            "publish_template": {
                "type": "template",
                "default": None,
                "description": "Template path for published work files. Should"
                "correspond to a template defined in templates.yml.",
            }
        }

        # update the base settings
        base_settings.update(nuke_publish_settings)

        return base_settings

    @property
    def item_filters(self):
        """
        List of item types that this plugin is interested in.

        Only items matching entries in this list will be presented to the
        accept() method. Strings can contain glob patters such as *, for example
        ["maya.*", "file.maya"]
        """
        return ["nuke.workfile"]

    def accept(self, settings, item):
        """
        Method called by the publisher to determine if an item is of any
        interest to this plugin. Only items matching the filters defined via the
        item_filters property will be presented to this method.

        A publish task will be generated for each item accepted here. Returns a
        dictionary with the following booleans:

            - accepted: Indicates if the plugin is interested in this value at
                all. Required.
            - enabled: If True, the plugin will be enabled in the UI, otherwise
                it will be disabled. Optional, True by default.
            - visible: If True, the plugin will be visible in the UI, otherwise
                it will be hidden. Optional, True by default.
            - checked: If True, the plugin will be checked in the UI, otherwise
                it will be unchecked. Optional, True by default.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process

        :returns: dictionary with boolean keys accepted, required and enabled
        """

        # if a publish template is configured, disable context change. This
        # is a temporary measure until the publisher handles context switching
        # natively.
        if settings.get("publish_template").value:
            item.context_change_allowed = False

        path = _session_path()

        if not path:
            # the session has not been saved before (no path determined).
            # provide a save button. the session will need to be saved before
            # validation will succeed.
            self.logger.warn(
                "The Nuke script has not been saved.", extra=_get_save_as_action()
            )

        self.logger.info(
            "Nuke '{}' plugin accepted the current Nuke script.".format(self.name)
        )
        return {"accepted": True, "checked": True}

    def validate(self, settings, item):
        """
        Validates the given item to check that it is ok to publish. Returns a
        boolean to indicate validity.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process
        :returns: True if item is valid, False otherwise.
        """

        # this method will handle validation specific to the nuke script itself.
        # the base class plugin will handle validation of the file itself

        publisher = self.parent
        path = _session_path()

        # by default, this will tell the publisher to version up the script on publish
        item.properties["version_up"] = True

        # ---- ensure the session has been saved

        if not path:
            # the session still requires saving. provide a save button.
            # validation fails.
            error_msg = "The Nuke script has not been saved."
            self.logger.error(error_msg, extra=_get_save_as_action())
            raise Exception(error_msg)

        # ---- check the session against any attached work template

        # get the path in a normalized state. no trailing separator,
        # separators are appropriate for current os, no double separators,
        # etc.
        path = sgtk.util.ShotgunPath.normalize(path)

        # if the session item has a known work template, see if the path
        # matches. if not, warn the user and provide a way to save the file to
        # a different path
        work_template = item.properties.get("work_template")
        if work_template:
            if not work_template.validate(path):
                self.logger.warning(
                    "The current session does not match the configured work "
                    "template.",
                    extra={
                        "action_button": {
                            "label": "Save File",
                            "tooltip": "Save the current Nuke session to a "
                            "different file name",
                            # will launch wf2 if configured
                            "callback": _get_save_as_action(),
                        }
                    },
                )
            else:
                self.logger.debug("Work template configured and matches session file.")
        else:
            self.logger.debug("No work template configured.")

        # ---- see if the version can be bumped post-publish

        # check to see if the next version of the work file already exists on
        # disk. if so, warn the user and provide the ability to save to the next
        # available version now
        (next_version_path, version) = self._get_next_version_info(path, item)
        if next_version_path and os.path.exists(next_version_path):

            # lets NOT version up if the next version already exists
            item.properties["version_up"] = False

            # determine the next available version_number. just keep asking for
            # the next one until we get one that doesn't exist.
            while os.path.exists(next_version_path):
                (next_version_path, version) = self._get_next_version_info(
                    next_version_path, item
                )

            warning_msg = "The next version of this file already exists on disk so this file will not automatically version up unless you save to next available version."
            self.logger.warning(
                warning_msg,
                extra={
                    "action_button": {
                        "label": "Save to v{}".format(version),
                        "tooltip": "Save to the next available version number, "
                        "v{}".format(version),
                        "callback": lambda: _save_and_allow_version_up(
                            next_version_path, item
                        ),
                    }
                },
            )

        # ---- populate the necessary properties and call base class validation

        # populate the publish template on the item if found
        publish_template_setting = settings.get("publish_template")
        publish_template = publisher.engine.get_template_by_name(
            publish_template_setting.value
        )
        if publish_template:
            item.properties["publish_template"] = publish_template

        # set the session path on the item for use by the base plugin validation
        # step. NOTE: this path could change prior to the publish phase.
        item.properties["path"] = path

        # run the base class validation
        return super(NukeWorkfilePublishPlugin, self).validate(settings, item)

    def publish(self, settings, item):
        """
        Executes the publish logic for the given item and settings.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process
        """

        # get the path in a normalized state. no trailing separator, separators
        # are appropriate for current os, no double separators, etc.
        path = sgtk.util.ShotgunPath.normalize(_session_path())

        # ensure the session is saved
        _save_session(path)

        # update the item with the saved session path
        item.properties["path"] = path

        # add dependencies for the base class to register when publishing
        item.properties[
            "publish_dependencies"
        ] = _nuke_find_additional_script_dependencies()

        # let the base class register the publish
        super(NukeWorkfilePublishPlugin, self).publish(settings, item)

    def finalize(self, settings, item):
        """
        Execute the finalization pass. This pass executes once all the publish
        tasks have completed, and can for example be used to version up files.

        :param settings: Dictionary of Settings. The keys are strings, matching
            the keys returned in the settings property. The values are `Setting`
            instances.
        :param item: Item to process
        """

        # do the base class finalization
        super(NukeWorkfilePublishPlugin, self).finalize(settings, item)

        if item.properties.get("version_up"):
            # bump the session file to the next version
            self._save_to_next_version(item.properties["path"], item, _save_session)


def _nuke_find_additional_script_dependencies():
    """
    Find all dependencies for the current nuke script
    """

    # figure out all the inputs to the scene and pass them as dependency
    # candidates
    dependency_paths = []
    for read_node in nuke.allNodes("Read"):
        # make sure we have a file path and normalize it
        # file knobs set to "" in Python will evaluate to None. This is
        # different than if you set file to an empty string in the UI, which
        # will evaluate to ""!
        file_path = read_node.knob("file").evaluate()
        if not file_path:
            continue
        file_path = sgtk.util.ShotgunPath.normalize(file_path)
        dependency_paths.append(file_path)

    return dependency_paths


def _save_and_allow_version_up(path, item):
    """
    Save the current session to the supplied path.
    """
    _save_session(path)
    item.properties["version_up"] = True


def _save_session(path):
    """
    Save the current session to the supplied path.
    """
    # Nuke won't ensure that the folder is created when saving, so we must make sure it exists
    ensure_folder_exists(os.path.dirname(path))

    nuke.scriptSaveAs(path, True)


def _session_path():
    """
    Return the path to the current session
    :return:
    """
    root_name = nuke.root().name()
    return None if root_name == "Root" else root_name


def _get_save_as_action():
    """
    Simple helper for returning a log action dict for saving the session
    """

    engine = sgtk.platform.current_engine()

    # default save callback
    callback = nuke.scriptSaveAs

    # if workfiles2 is configured, use that for file save
    if "tk-multi-workfiles2" in engine.apps:
        app = engine.apps["tk-multi-workfiles2"]
        if hasattr(app, "show_file_save_dlg"):
            callback = app.show_file_save_dlg

    return {
        "action_button": {
            "label": "Save As..",
            "tooltip": "Save the current session",
            "callback": callback,
        }
    }
