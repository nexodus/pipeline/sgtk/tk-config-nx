﻿# Copyright (c) 2017 Shotgun Software Inc.
#
# CONFIDENTIAL AND PROPRIETARY
#
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights
# not expressly granted therein are reserved by Shotgun Software Inc.

import os
import nuke
import sgtk

HookBaseClass = sgtk.get_hook_baseclass()

_NUKE_OUTPUTS = {
    "Write": {"path": "file", "publish_type": "Nuke Render"},
    "WriteGeo": {"path": "file", "publish_type": None},
    "GenerateLUT": {"path": "file", "publish_type": "Color LUT"},
}


class NukeSessionCollector(HookBaseClass):
    """
    Collector that operates on the current nuke/nukestudio session. Should
    inherit from the basic collector hook.
    """

    @property
    def settings(self):
        """
        Dictionary defining the settings that this collector expects to receive
        through the settings parameter in the process_current_session and
        process_file methods.

        A dictionary on the following form::

            {
                "Settings Name": {
                    "type": "settings_type",
                    "default": "default_value",
                    "description": "One line description of the setting"
            }

        The type string should be one of the data types that toolkit accepts as
        part of its environment configuration.
        """

        # grab any base class settings
        collector_settings = super(NukeSessionCollector, self).settings or {}

        # settings specific to this collector
        nuke_session_settings = {
            "work_template": {
                "type": "template",
                "default": None,
                "description": "Template path for artist work files. Should "
                "correspond to a template defined in "
                "templates.yml. If configured, is made available"
                "to publish plugins via the collected item's "
                "properties. ",
            },
        }

        # update the base settings with these settings
        collector_settings.update(nuke_session_settings)

        return collector_settings

    def process_current_session(self, settings, parent_item):
        """
        Analyzes the current session open in Nuke and parents a
        subtree of items under the parent_item passed in.

        :param dict settings: Configured settings for this collector
        :param parent_item: Root item instance
        """

        publisher = self.parent
        engine = publisher.engine

        if (hasattr(engine, "studio_enabled") and engine.studio_enabled) or (
            hasattr(engine, "hiero_enabled") and engine.hiero_enabled
        ):

            # running nuke studio or hiero
            project_items = self.collect_current_hiero_session(settings, parent_item)

        else:
            # running nuke. ensure additional collected outputs are parented
            # under the session
            session_item = self.collect_current_nuke_session(settings, parent_item)
            self.collect_sg_writenodes(settings, session_item)
            self.collect_node_outputs(settings, session_item)
            self.collect_selected_read_nodes(settings, session_item)

    def collect_current_nuke_session(self, settings, parent_item):
        """
        Analyzes the current session open in Nuke and parents a subtree of items
        under the parent_item passed in.

        :param dict settings: Configured settings for this collector
        :param parent_item: Root item instance
        """

        publisher = self.parent

        # get the current path
        path = _session_path()

        if not path:
            self.logger.error(
                "Your current Nuke session could not be collected. You "
                "need to save your script to a proper workfile location."
            )
            return None

        # get the frame range for publisher validation
        first_frame = int(nuke.root()["first_frame"].value())
        last_frame = int(nuke.root()["last_frame"].value())

        # let the base collector get the session item
        session_item = super(NukeSessionCollector, self)._collect_file(
            settings, parent_item, path
        )

        if session_item:

            session_item.type_spec = "nuke.workfile"
            session_item.type_display = "Nuke Workfile"  # for UI only
            session_item.properties[
                "publish_type"
            ] = "Nuke Workfile"  # for publish entity in SG
            session_item.properties["session_frame_range"] = (first_frame, last_frame)

            self.logger.info("Collected current Nuke Workfile")
            return session_item

        self.logger.error("Your current Nuke session could not be collected.")
        return None

    def collect_current_hiero_session(self, settings, parent_item):
        """
        Analyzes the current session open in NukeStudio/Hiero and collects
        all the projects it finds.
        :param dict settings: Configured settings for this collector
        :param parent_item: Root item instance
        """

        # import hiero here since this portion only runs in hiero.
        import hiero.core

        publisher = self.parent

        # make a list of project items to return
        project_items = []

        for project in hiero.core.projects():

            # only want projects with paths
            if project:

                # let the base collector get the project item
                project_item = super(NukeSessionCollector, self)._collect_file(
                    settings, parent_item, project.path()
                )

                project_item.type_spec = "hiero.workfile"
                project_item.type_display = "Hiero Workfile"  # for UI only
                project_item.properties["project"] = project
                project_item.properties[
                    "publish_type"
                ] = "Hiero Workfile"  # for publish entity in SG

                self.logger.info("Collected current Hiero Workfile")

                project_items.append(project_item)

        return project_items

    def collect_node_outputs(self, settings, parent_item):
        """
        Scan known output node types in the session and see if they reference
        files that have been written to disk.

        :param parent_item: The parent item for any nodes collected
        """
        nuke_views = nuke.views()

        # iterate over all the known output types
        for node_type in _NUKE_OUTPUTS:

            # get all the instances of the node type
            all_nodes_of_type = [n for n in nuke.allNodes() if n.Class() == node_type]

            # iterate over each instance
            for node in all_nodes_of_type:

                params = _NUKE_OUTPUTS[node_type]

                for nuke_view in nuke_views:

                    # first we create an OutputContext for the current view
                    oc = nuke.OutputContext()
                    oc.setView(oc.viewFromName(nuke_view))

                    # this evaluates the path for the nuke_view - frames will be 0
                    file_path = node[params["path"]].toScript(False, context=oc)

                    self.logger.info(
                        "Processing {} node: {}".format(node_type, node.name())
                    )

                    # file exists, let the basic collector handle it
                    item = super(NukeSessionCollector, self)._collect_file(
                        settings, parent_item, file_path
                    )

                    if item:
                        # the item has been created. update the display name to include
                        # the nuke node to make it clear to the user how it was
                        # collected within the current session.
                        item.name = "{}: {}".format(node.name(), item.name)
                        if node.Class() == "Write":
                            item.properties["width_height"] = (
                                node.width(),
                                node.height(),
                            )

    def collect_selected_read_nodes(self, settings, parent_item):
        """
        Scan selected read nodes in case the user wants to publish them.

        :param parent_item: The parent item for any nodes collected
        """

        nuke_views = nuke.views()

        # get all the selected Reads
        selected_reads = [n for n in nuke.selectedNodes() if n.Class() == "Read"]

        # iterate over each instance
        for node in selected_reads:

            for nuke_view in nuke_views:

                # first we create an OutputContext for the current view
                oc = nuke.OutputContext()
                oc.setView(oc.viewFromName(nuke_view))

                # this evaluates the path for the nuke_view - frames will be 0
                file_path = node["file"].toScript(False, context=oc)

                self.logger.info(
                    "Processing selected Read node: {}".format(node.name())
                )

                # file exists, let the basic collector handle it
                item = super(NukeSessionCollector, self)._collect_file(
                    settings, parent_item, file_path
                )

                if item:
                    # the item has been created. update the display name to include
                    # the nuke node to make it clear to the user how it was
                    # collected within the current session.
                    item.name = "{}: {}".format(node.name(), item.name)
                    # store the width and hight from the node
                    item.properties["width_height"] = (node.width(), node.height())

    def collect_sg_writenodes(self, settings, parent_item):
        """
        Collect any rendered sg write nodes in the session.

        :param parent_item:  The parent item for any sg write nodes collected
        """

        publisher = self.parent
        engine = publisher.engine

        sg_writenode_app = engine.apps.get("tk-nuke-writenode")
        if not sg_writenode_app:
            self.logger.debug(
                "The tk-nuke-writenode app is not installed. "
                "Will not attempt to collect those nodes."
            )
            return

        # make sure we check all available views
        nuke_views = nuke.views()

        for node in sg_writenode_app.get_write_nodes():

            render_path = sg_writenode_app.get_node_render_path(node)

            for nuke_view in nuke_views:

                view_render_path = render_path.replace("%V", nuke_view)

                # file exists, let the basic collector handle it
                item = super(NukeSessionCollector, self)._collect_file(
                    settings, parent_item, view_render_path
                )

                if item:
                    # append the node name to the beginning of the item so
                    # the user knows where it was collected from. This is
                    # just for the UI
                    item.name = "{}: {}".format(node.name(), item.name)
                    item.properties["width_height"] = (node.width(), node.height())


def _session_path():
    """
    Return the path to the current session
    :return:
    """
    root_name = nuke.root().name()
    return None if root_name == "Root" else root_name
